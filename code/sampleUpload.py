# -*- coding: utf-8 -*-
"""
Created on Fri Sep  9 13:17:08 2022

@author: JESSICA.THOMPSON
This script is run after sample list is created using TX_RES_Sampling.py 
and TX_RES_Concat_Sampling.py scripts 
"""

#%% set data directory and variables for residential samples 

import pandas as pd 
import pyodbc 
from sqlalchemy import create_engine


dbSchema = 'TX2022'
# open connection
conn = pyodbc.connect('Driver={SQL Server};Server=etss704sqldevqa,50179;Database=TX_EMV_PUCT;Trusted_Connection=yes;')
cursor = conn.cursor()
# create sqlalchemy engine
con_str = 'mssql+pyodbc://etss704sqldevqa:50179/'+'TX_EMV_PUCT'+'?trusted_connection=yes&driver=SQL Server Native Client 11.0'
engine = create_engine(con_str, fast_executemany=True)

#%% read in .xlsx table data and prepare for upload 

# read in and concat residential desk review samples 

loadDataDir = "F://Texas PUC 2021-2024/Sample/PY2022/Residential/"
fileNames = ['AEP_Texas/AEP_Total_SamplesV2.xlsx','CenterPoint/CP_Total Samples.xlsx',
             'El_Paso/El_Paso_Total Samples.xlsx','Entergy/EPE_Final_Samples.xlsx',
             'Oncor/Oncor_Total SamplesV2.xlsx','SWEPCO/SWEPCO_Total_SamplesV2.xlsx',
             'TNMP/TNMP_Total_SamplesV2.xlsx','Xcel/Xcel_Final_SamplesV2.xlsx']
tableNameRes = 'ResSampleDataQ1Q2' 

dataList = []
for file in fileNames:
    print(file)
    df = pd.read_excel(loadDataDir+file,sheet_name='Sample_Attributes_DR')
    dataList.append(df)
sampleData = pd.concat(dataList)

colsKeep = ['UtilityProgramName','participant_id','participant_code','esiid','customer_name',
            'mail_address1','program_year_id','UtilityName','ProgramYear','SectorName',
            'ReportSectorDescription','measure_reporting_description','source_measure_code',
            'MeasureCategoryName','project_id','source_data_row_id','participant_impact_date',
            'count','quantity','incentive','exante_savings_kw','exante_savings_kwh','UtilityID',
            'SectorID','measure_id','quantity_unit','eul','ReportSectorID']

# remove extra rows and keep required columns
sampleData = sampleData.loc[~sampleData['UtilityProgramName'].isnull(),colsKeep]
sampleData['sampleType'] = 'desk_review'

# read in and concat residential onsite samples 
dataList = []
for file in fileNames:
    print(file)
    df = pd.read_excel(loadDataDir+file,sheet_name='Sample_Attributes_OS')
    dataList.append(df)
sampleDataOS = pd.concat(dataList)

colsKeep = ['UtilityProgramName','participant_id','participant_code','esiid','customer_name',
            'mail_address1','program_year_id','UtilityName','ProgramYear','SectorName',
            'ReportSectorDescription','measure_reporting_description','source_measure_code',
            'MeasureCategoryName','project_id','source_data_row_id','participant_impact_date',
            'count','quantity','incentive','exante_savings_kw','exante_savings_kwh','UtilityID',
            'SectorID','measure_id','quantity_unit','eul','ReportSectorID']

# remove extra rows and keep required columns
sampleDataOS = sampleDataOS.loc[~sampleDataOS['UtilityProgramName'].isnull(),colsKeep]
sampleDataOS['sampleType'] = 'onsite'

# concat data 
sampleDataAll = pd.concat([sampleData,sampleDataOS])

#%% check to see if table exists, if not, upload table 

# check if exists 
sql_query = """
select
    count(*)
from
    INFORMATION_SCHEMA.VIEWS
where
    table_name = ? 
    and table_schema = ?
"""

cursor.execute(sql_query, tableNameRes, dbSchema)
results = cursor.fetchall()

if results[0][0]>0:
    print('Table '+tableNameRes+' already exists')
else:
    sampleDataAll.to_sql(tableNameRes, con = engine, index=False, schema=dbSchema, if_exists='replace')
    print(str(len(sampleDataAll))+' rows inserted to '+dbSchema+'.'+tableNameRes)
    conn.commit()

#%% same as above for Commercial

loadDataDir = "F://Texas PUC 2021-2024/Sample/PY2022/Commercial/Q1-4 Desk Review Sample/Q1-2/"
fileNames = ['2022 Q1-2 Commercial Sampled Projects_final_version.xlsx']
tableNameComm = 'CommSampleDataQ1Q2' 

#%% read in .xlsx table data and prepare for upload 

# read in and concat commercial desk review samples 
dataList = []
for file in fileNames:
    print(file)
    df = pd.read_excel(loadDataDir+file)
    dataList.append(df)
sampleData = pd.concat(dataList)

sampleData['sampleType'] = 'desk_review'

# no onsite samples this quarter 
sampleDataAll = sampleData

#%% check to see if table exists, if not, upload table 

# check if exists 
sql_query = """
select
    count(*)
from
    INFORMATION_SCHEMA.VIEWS
where
    table_name = ? 
    and table_schema = ?
"""

cursor.execute(sql_query, tableNameComm, dbSchema)
results = cursor.fetchall()

if results[0][0]>0:
    print('Table '+tableNameComm+' already exists')
else:
    sampleDataAll.to_sql(tableNameComm, con = engine, index=False, schema=dbSchema, if_exists='replace')
    print(str(len(sampleDataAll))+' rows inserted to '+dbSchema+'.'+tableNameComm)
    conn.commit()

# which fields does Graham want access to? most recent? values at time we requested info? 

#%% drop if exists and create view deskReviewSampleList 

sql_statement1 = """
.deskReviewSampleListQ1Q2 
as 
select cie.participant_id, cie.program_year_id, sl.esiid, sl.project_id, 
sl.participant_impact_date, sl.source_measure_code, sl.MeasureCategoryName, 
sl.exante_savings_kw, sl.exante_savings_kwh, sl.participant_code, sl.customer_name, 
sl.mail_address1, sl.sampleType, sl.participant_id_sample from (
SELECT esiid, UtilityName, UtilityProgram as UtilityProgramName, SectorName, project_id, 
participant_impact_date, source_measure_code, MeasureCategoryName, exante_savings_kw, 
exante_savings_kwh, participant_code, customer_name, mail_address1, sampleType, 
null as participant_id_sample
FROM TX_EMV_PUCT.
"""

sql_statement2 = """
 union all SELECT esiid, UtilityName, UtilityProgramName, SectorName, project_id, 
participant_impact_date, source_measure_code, MeasureCategoryName, exante_savings_kw, 
exante_savings_kwh, participant_code, customer_name, mail_address1, sampleType, 
participant_id as participant_id_sample 
FROM TX_EMV_PUCT.
"""

sql_statement3 = """
) sl left join (select distinct program_year_id, participant_id, esiid, project_id,  
participant_code from TX_EMV_PUCT.
"""

sql_statement4 = """
.CustomerImpactExport) cie 
on coalesce(sl.esiid,'na')=coalesce(cie.esiid,'na') and sl.project_id=cie.project_id 
and sl.participant_code=cie.participant_code 
where sl.sampleType='desk_review' 
"""


# check if exists 
sql_query = """
select
    count(*)
from
    INFORMATION_SCHEMA.VIEWS
where
    table_name = 'deskReviewSampleListQ1Q2'
    and table_schema = ?
"""

cursor.execute(sql_query, dbSchema)
results = cursor.fetchall()

if results[0][0]>0:
    sql_statement = 'alter view '+dbSchema+sql_statement1+dbSchema+'.'+tableNameComm+sql_statement2+dbSchema+'.'+tableNameRes+sql_statement3+dbSchema+sql_statement4
    cursor.execute(sql_statement)
    conn.commit()
else:
    sql_statement = 'create view '+dbSchema+sql_statement1+dbSchema+'.'+tableNameComm+sql_statement2+dbSchema+'.'+tableNameRes+sql_statement3+dbSchema+sql_statement4
    cursor.execute(sql_statement)
    conn.commit()
    
#%% drop if exists and create view onsiteSampleList 

sql_statement1 = """
.onsiteSampleListQ1Q2 
as 
select cie.participant_id, cie.program_year_id, sl.esiid, sl.project_id, 
sl.participant_impact_date, sl.source_measure_code, sl.MeasureCategoryName, 
sl.exante_savings_kw, sl.exante_savings_kwh, sl.participant_code, sl.customer_name, 
sl.mail_address1, sl.sampleType, sl.participant_id_sample from (
SELECT esiid, UtilityName, UtilityProgram as UtilityProgramName, SectorName, project_id, 
participant_impact_date, source_measure_code, MeasureCategoryName, exante_savings_kw, 
exante_savings_kwh, participant_code, customer_name, mail_address1, sampleType, 
null as participant_id_sample
FROM TX_EMV_PUCT.
"""

sql_statement2 = """
 union all SELECT esiid, UtilityName, UtilityProgramName, SectorName, project_id, 
participant_impact_date, source_measure_code, MeasureCategoryName, exante_savings_kw, 
exante_savings_kwh, participant_code, customer_name, mail_address1, sampleType, 
participant_id as participant_id_sample 
FROM TX_EMV_PUCT.
"""

sql_statement3 = """
) sl left join (select distinct program_year_id, participant_id, esiid, project_id,  
participant_code from TX_EMV_PUCT.
"""

sql_statement4 = """
.CustomerImpactExport) cie 
on coalesce(sl.esiid,'na')=coalesce(cie.esiid,'na') and sl.project_id=cie.project_id 
and sl.participant_code=cie.participant_code 
where sl.sampleType='onsite' 
"""


# check if exists 
sql_query = """
select
    count(*)
from
    INFORMATION_SCHEMA.VIEWS
where
    table_name = 'onsiteSampleListQ1Q2'
    and table_schema = ?
"""

cursor.execute(sql_query, dbSchema)
results = cursor.fetchall()

if results[0][0]>0:
    sql_statement = 'alter view '+dbSchema+sql_statement1+dbSchema+'.'+tableNameComm+sql_statement2+dbSchema+'.'+tableNameRes+sql_statement3+dbSchema+sql_statement4
    cursor.execute(sql_statement)
    conn.commit()
else:
    sql_statement = 'create view '+dbSchema+sql_statement1+dbSchema+'.'+tableNameComm+sql_statement2+dbSchema+'.'+tableNameRes+sql_statement3+dbSchema+sql_statement4
    cursor.execute(sql_statement)
    conn.commit()


