# -*- coding: utf-8 -*-
"""
Created on Fri Jan 28 12:46:23 2022

@author: JESSICA.THOMPSON
The updateUtilityInputs.py script must be run before this script.
This script uploads the prepared ProgramYear table to the TX EMV PUCT database. 
Prior to running, the Excel CreateProgramYear template must be completed. 
Prior to running, update loadDataDir, fileName, and dbSchema with values for this PY
"""

#%% set data directory and variables

import pandas as pd 
import pyodbc 
from sqlalchemy import create_engine
import datetime

loadDataDir = "F://Texas PUC 2021-2024/EMV Database/2022etl/data/"
fileName = 'CreateProgramYear.xlsx'
dbSchema = 'TX2022'

#%% read in .xlsx table data and prepare for upload 

programYear = pd.read_excel(loadDataDir+fileName,sheet_name='ProgramYear')

colsKeep = ['ProgramID','UtilityID','ProgramYear','ReportSectorID','UtilityProgramName',
            'LeadEvaluator','EvaluationPriority','PrecisionRankID','EEPR_kW','EEPR_kWh',
            'Incentives','AdminCosts','RDCosts','EMVCosts','SortOrder','SectorID']

# remove extra rows and keep required columns
programYear = programYear.loc[~programYear['UtilityProgramName'].isnull(),colsKeep]

# convert relevant columns to numeric
programYear['EEPR_kW'] = programYear['EEPR_kW'].apply(lambda x: x.replace(',','').replace('$','') if isinstance(x,str) else x)
programYear['EEPR_kWh'] = programYear['EEPR_kWh'].apply(lambda x: x.replace(',','').replace('$','') if isinstance(x,str) else x)
programYear['Incentives'] = programYear['Incentives'].apply(lambda x: x.replace(',','').replace('$','') if isinstance(x,str) else x)
programYear['AdminCosts'] = programYear['AdminCosts'].apply(lambda x: x.replace(',','').replace('$','') if isinstance(x,str) else x)
programYear['RDCosts'] = programYear['RDCosts'].apply(lambda x: x.replace(',','').replace('$','') if isinstance(x,str) else x)
programYear['EMVCosts'] = programYear['EMVCosts'].apply(lambda x: x.replace(',','').replace('$','') if isinstance(x,str) else x)
programYear['EEPR_kW'] = pd.to_numeric(programYear['EEPR_kW'],errors='coerce')
programYear['EEPR_kWh'] = pd.to_numeric(programYear['EEPR_kWh'],errors='coerce')
programYear['Incentives'] = pd.to_numeric(programYear['Incentives'],errors='coerce')
programYear['AdminCosts'] = pd.to_numeric(programYear['AdminCosts'],errors='coerce')
programYear['RDCosts'] = pd.to_numeric(programYear['RDCosts'],errors='coerce')
programYear['EMVCosts'] = pd.to_numeric(programYear['EMVCosts'],errors='coerce')

# replace multiple spaces in program names with one 
programYear['UtilityProgramName'] = programYear['UtilityProgramName'].apply(lambda x: ' '.join(x.split()))


#%% check to see if data exists in current ProgramYear table and match w/ template data 

# open connection
conn = pyodbc.connect('Driver={SQL Server};Server=etss704sqldevqa,50179;Database=TX_EMV_PUCT;Trusted_Connection=yes;')

cursor = conn.cursor()
# pull matches and make sure they exist 
sql_statement = """
select ProgramYearID, ProgramID, UtilityID, ProgramYear, ReportSectorID, SectorID,
UtilityProgramName, LeadEvaluator, EvaluationPriority, PrecisionRankID, 
EEPR_kW, EEPR_kWh, Incentives, AdminCosts, RDCosts, EMVCosts, createdDate, modifiedDate  
from 
"""
existingPrograms = pd.read_sql(sql_statement+dbSchema+'.ProgramYear', conn)

programsExisting = programYear.merge(
    existingPrograms[['UtilityID','UtilityProgramName','ReportSectorID','SectorID','ProgramYearID','createdDate']],
    on=['UtilityID','UtilityProgramName','ReportSectorID','SectorID'],how='outer',indicator=True)

# investigate any programs in data file not matching db programs 
print('show any Programs in data file with no match in DB')
print(programsExisting.loc[programsExisting['ProgramYearID'].isnull(),])

# investigate any programs in db w/o a match in the file 
print('these programs are in the db, but not in the file being uploaded')
missingPrograms = programsExisting.loc[programsExisting['_merge']=='right_only',
                                       ['ProgramYearID']].merge(existingPrograms,on='ProgramYearID')
print(missingPrograms[['UtilityID','ProgramYearID','UtilityProgramName','EEPR_kW','EEPR_kWh']])

#%% divide out into updates and inserts 

# inserts 
programYearInserts = programsExisting.loc[programsExisting['ProgramYearID'].isnull(),colsKeep]
programYearInserts['createdDate'] = datetime.datetime.now()
programYearInserts['modifiedDate'] = datetime.datetime.now()
print('prepared '+str(len(programYearInserts))+' records for insertion')

# updates 
programYearUpdates = programsExisting.loc[~programsExisting['ProgramYearID'].isnull(),]
programYearUpdates['modifiedDate'] = datetime.datetime.now()
print('prepared '+str(len(programYearUpdates))+' records for update')

#%% write updates and inserts to database 

# create sqlalchemy engine
con_str = 'mssql+pyodbc://etss704sqldevqa:50179/'+'TX_EMV_PUCT'+'?trusted_connection=yes&driver=SQL Server Native Client 11.0'
engine = create_engine(con_str, fast_executemany=True)

# inserts 
if len(programYearInserts)>0:
    print(datetime.datetime.now())
    programYearInserts.to_sql('ProgramYear', con = engine, index=False, schema=dbSchema, if_exists='append')
    print(datetime.datetime.now())
    print(str(len(programYearInserts))+' rows inserted to '+dbSchema+'.ProgramYear')
    conn.commit()

if len(programYearUpdates)>0:
    programYearUpdates.to_sql('tempUpdates', con = engine, index=False, schema=dbSchema, if_exists='replace')
    conn.commit()
    # run merge statement 
    sqlStatement = """
    .tempUpdates as source 
    on source.ProgramYearID=target.ProgramYearID 
    when matched then update set  
    target.ProgramYear=source.ProgramYear, 
    target.LeadEvaluator=source.LeadEvaluator,
    target.EvaluationPriority=source.EvaluationPriority, 
    target.PrecisionRankID=source.PrecisionRankID, 
    target.EEPR_kW=source.EEPR_kW, 
    target.EEPR_kWh=source.EEPR_kWh, 
    target.Incentives=source.Incentives, 
    target.AdminCosts=source.AdminCosts, 
    target.RDCosts=source.RDCosts, 
    target.EMVCosts=source.EMVCosts, 
    target.modifiedDate=source.modifiedDate,
    target.sortOrder=source.sortOrder;
    """
    cursor.execute('merge '+dbSchema+'.ProgramYear as target using '+dbSchema+sqlStatement)
    conn.commit()
    cursor.execute('drop table '+dbSchema+'.tempUpdates;')
    conn.commit()
    print(str(len(programYearUpdates))+' rows updated in '+dbSchema+'.ProgramYear')
    conn.commit()

#%% drop if exists and create view ProgramYearList for current ProgramYear 

sql_statement1 = """
.ProgramYearList 
as 
SELECT PY.ProgramYearID, PY.UtilityID, U.UtilityName, U.UtilityDescription, PY.LeadEvaluator, PY.ProgramYear, PY.UtilityProgramName, 
PY.Precision_kW, PY.Precision_kWh, PY.StandardError_kW, PY.StandardError_kWh, 
PY.PrecisionRankID, PR.PrecisionRankName, PRG.ProgramID, PRG.ProgramName, PRG.ProgramDescription, PY.SectorID, PRG.ProgramTypeID, 
PRG.ProgramGroupID, PRG.ReportDisplayOrder, S.SectorName, PT.ProgramTypeName, PG.ProgramGroupName,
PY.EEPR_kW, PY.EEPR_kWh, PY.ReportSectorID, RS.ReportSectorDescription, RS.ReportSectorSort, PY.ProgramSortOrder, 
PY.EvaluationPriority, PY.AdminCosts, PY.Incentives, PY.RDCosts, PY.EMVCosts, PY.NTG_kWh, 
PY.NTG_kW, PY.TStat, PY.Population, PY.SortOrder
FROM TX_EMV_PUCT.
"""

sql_statement2 = """
.ProgramYear AS PY INNER JOIN
TX_EMV_PUCT.lookup.Program AS PRG ON PY.ProgramID = PRG.ProgramID INNER JOIN
TX_EMV_PUCT.lookup.Sector AS S ON PY.SectorID = S.SectorID INNER JOIN
TX_EMV_PUCT.lookup.ProgramType AS PT ON PRG.ProgramTypeID = PT.ProgramTypeID INNER JOIN
TX_EMV_PUCT.lookup.ProgramGroup AS PG ON PRG.ProgramGroupID = PG.ProgramGroupID INNER JOIN
TX_EMV_PUCT.lookup.Utility AS U ON PY.UtilityID = U.UtilityID LEFT OUTER JOIN
TX_EMV_PUCT.lookup.PrecisionRank AS PR ON PY.PrecisionRankID = PR.PrecisionRankID LEFT OUTER JOIN
TX_EMV_PUCT.lookup.ReportSector AS RS ON PY.ReportSectorID = RS.ReportSectorID
"""

# check if exists 
sql_query = """
select
    count(*)
from
    INFORMATION_SCHEMA.VIEWS
where
    table_name = 'ProgramYearList'
    and table_schema = ?
"""

cursor.execute(sql_query, dbSchema)
results = cursor.fetchall()

if results[0][0]>0:
    sql_statement = 'alter view '+dbSchema+sql_statement1+dbSchema+sql_statement2
    cursor.execute(sql_statement)
    conn.commit()
else:
    sql_statement = 'create view '+dbSchema+sql_statement1+dbSchema+sql_statement2
    cursor.execute(sql_statement)
    conn.commit()


#%% drop if exists and create view ProgramCostAllocationEEPRs

sqlStatement1 = """
.ProgramCostAllocationEEPRs as 
select a.ProgramYearID, a.ProgramYear, a.UtilityID, p.UtilityName as Utility, 
p.SectorName as Sector, a.EvaluationPriority, a.priority_percent, 
p.UtilityProgramName as Program, p.SortOrder, 
a.kWh, a.kW, a.kWh_percent, a.kW_percent, a.Blended_percent, 
cast(ui.TotalAllocation as float)*cast(a.kWh_percent as float) as kWh_cost, 
cast(ui.TotalAllocation as float)*cast(a.priority_percent as float) as priority_cost, 
cast(ui.TotalAllocation as float)*cast(a.Blended_percent as float) as blended_cost, 
cast(ui.CY1Allocation as float)*cast(a.Blended_percent as float) as cy1_cost,
cast(ui.CY2Allocation as float)*cast(a.Blended_percent as float) as cy2_cost 
from (select p.ProgramYearID, p.ProgramYear, p.UtilityID, p.EvaluationPriority, 
case when p.EvaluationPriority='High' then 5.0 when p.EvaluationPriority='Medium' then 3.0 when p.EvaluationPriority='Low' then 1.0 else 0.0 end as PriorityWeight, 
case when p.EvaluationPriority='High' then 5.0 when p.EvaluationPriority='Medium' then 3.0 when p.EvaluationPriority='Low' then 1.0 else 0.0 end/weight_utility as priority_percent, 
cast(p.EEPR_kWh as float) as kWh, cast(p.EEPR_kW as float) as kW, 
cast(p.EEPR_kWh as float)/cast(u.kWh_utility as float) as kWh_percent, 
cast(p.EEPR_kW as float)/cast(u.kW_utility as float) as kW_percent, 
(cast(p.EEPR_kWh as float)/cast(u.kWh_utility as float)+case when p.EvaluationPriority='High' then 5.0 when p.EvaluationPriority='Medium' then 3.0 when p.EvaluationPriority='Low' then 1.0 else 0.0 end/weight_utility)/2 as Blended_percent 
from 
"""
sqlStatement2 = """
.ProgramYearList p 
inner join (select UtilityID,ProgramYear, sum(cast(EEPR_kWh as float)) as kWh_utility, sum(cast(EEPR_kW as float)) as kW_utility, 
sum(case when EvaluationPriority='High' then 5.0 when EvaluationPriority='Medium' then 3.0 when EvaluationPriority='Low' then 1.0 else 0.0 end) as weight_utility 
from 
"""
sqlStatement3 = """
.ProgramYearList 
group by UtilityID, ProgramYear) u on p.UtilityID=u.UtilityID) a  
inner join (select UtilityID, EMVCostAllocation, EMVBudget, 
EMVCostAllocation*EMVBudget as TotalAllocation, 
EMVCostAllocation*EMVBudget*CY1Allocation as CY1Allocation, 
EMVCostAllocation*EMVBudget*CY2Allocation as CY2Allocation 
from 
"""
sqlStatement4 = """
.UtilityInputs) ui on a.UtilityID=ui.UtilityID 
inner join 
"""
sqlStatement5 = """
.ProgramYearList p on a.ProgramYearID=p.ProgramYearID 
"""

# check if exists 
sql_query = """
select
    count(*)
from
    INFORMATION_SCHEMA.VIEWS
where
    table_name = 'ProgramCostAllocationEEPRs'
    and table_schema = ?
"""

cursor.execute(sql_query, dbSchema)
results = cursor.fetchall()

if results[0][0]>0:
    sql_statement = 'alter view '+dbSchema+sqlStatement1+dbSchema+sqlStatement2+dbSchema+sqlStatement3+dbSchema+sqlStatement4+dbSchema+sqlStatement5
    cursor.execute(sql_statement)
    conn.commit()
else:
    sql_statement = 'create view '+dbSchema+sqlStatement1+dbSchema+sqlStatement2+dbSchema+sqlStatement3+dbSchema+sqlStatement4+dbSchema+sqlStatement5
    cursor.execute(sql_statement)
    conn.commit()





