"""
Created on Thu Feb  4 15:05:16 2021

@author: JESSICA.THOMPSON
This script exports tables from the TX database for use in Quickbase 
Before running, must:
    1) Load utility data into the TX database (can be run with partial data loaded)
    2) Update file_dir (line 52) to desired location for Quickbase tables
 code to pull most recent TX PUCT data from sql server and output to .csv files for 
 import into Quickbase
 join conditions and where clauses use data model info from 
 F:\Texas PUC EM&V\EM&V Database\Documentation\Database Documentation.docx 
 and database view CustomerImpactExport
"""

#%% variables and imports 
import pandas as pd
import pyodbc

programYear = 2022
dbSchema = 'TX2022'
file_dir = 'F://Texas PUC 2021-2024/EMV Database/2022exports/quickbase_tables/'

conn = pyodbc.connect('Driver={SQL Server};Server=etss704sqldevqa,50179;Database=TX_EMV_PUCT;Trusted_Connection=yes;')
cursor = conn.cursor()

#%% define function 

# create function to take sql and directory imports, pull data, and write csv
def pull_write_data(conn,file_dir,table_sql,table_name):
    """
    Parameters
    ----------
    conn : sql server connection
        previously opened connection to the db.
    file_dir : string 
        string giving directory for files.
    table_sql : string
        string given sql query to be run.
    table_name : string
        name of table, used in export file naming.

    Returns
    -------
    None.
    """
    table_data = pd.read_sql(table_sql, conn)
    table_data.to_csv(file_dir+table_name+'.csv', index=False, quoting=2, line_terminator='\n')
    print('Wrote table '+table_name)

#%% pull data and write to files 
    
# run query for table program and write csv file
program_sql = """
select distinct ProgramID, ProgramName, ProgramDescription, 
SectorID, ProgramTypeID, ProgramGroupID from """
program_sql_all = program_sql+dbSchema+'.CustomerImpactExport'
table_name = 'program'
pull_write_data(conn,file_dir,program_sql_all,table_name)

# run query for table group and write csv file
group_sql = """
select distinct ProgramGroupID, ProgramGroupName from """
group_sql_all = group_sql+dbSchema+'.CustomerImpactExport'
table_name = 'group'
pull_write_data(conn,file_dir,group_sql_all,table_name)

# run query for table program_type and write csv file
type_sql = """
select distinct ProgramTypeID, ProgramTypeName from """
type_sql_all = type_sql+dbSchema+'.CustomerImpactExport' 
table_name = 'program_type'
pull_write_data(conn,file_dir,type_sql_all,table_name)

# run query for table sector and write csv file
sector_sql = """
select distinct SectorID, SectorName, SectorDescription from """
sector_sql_all = sector_sql+'lookup.Sector' 
table_name = 'sector'
pull_write_data(conn,file_dir,sector_sql_all,table_name)

# run query for combined table programyear_eval_priority_report and write csv file
program_year_sql = """
select ProgramYearID, ProgramID, UtilityID, LeadEvaluator, ProgramYear, 
UtilityProgramName, PrecisionRankName, ReportSectorID, ReportSectorDescription, 
SectorID, SectorName, 
EvaluationPriority, Precision_kW, Precision_kWh, StandardError_kW, StandardError_kWh, 
EEPR_kW, EEPR_kWh, NTG_kW, NTG_kWh, AdminCosts, RDCosts, EMVCosts, Incentives, 
TStat, Population 
from  """
program_year_sql_all = program_year_sql+dbSchema+'.ProgramYearList'
table_name = 'programyear_eval_priority_report'
pull_write_data(conn,file_dir,program_year_sql_all,table_name)

# run query for utility table
utility_sql = """
select distinct UtilityId, UtilityName, UtilityDescription from """
utility_sql_all = utility_sql+'lookup.Utility' 
table_name = 'utility'
pull_write_data(conn,file_dir,utility_sql_all,table_name)

# run query for combined table measure and write to csv file 
measure_sql = """
select distinct cie.measure_id, cie.program_year_id, cie.SectorID, 
cie.measure_category_id, cie.measure_description, cie.measure_reporting_description, 
cie.source_measure_code, m.realization_rate_kw, m.realization_rate_kwh, 
cie.eul from """
measure_sql_all = measure_sql+dbSchema+'.CustomerImpactExport cie join '+dbSchema+'.Measure m on cie.measure_id=m.measure_id' 
table_name = 'measure' 
pull_write_data(conn, file_dir, measure_sql_all, table_name)

# run query for table category
category_sql = """
select distinct MeasureCategoryID, MeasureCategoryName from """
category_sql_all = category_sql+'lookup.MeasureCategory'
table_name = 'category'
pull_write_data(conn,file_dir,category_sql_all,table_name)

# run query for combined table participant_customer_account and write to csv file 
participant_sql = """
select distinct participant_id, participant_code, participant_code_source, 
program_year_id, esiid, customer_name, email_address, mail_address1, 
mail_address2, mail_city, mail_state, mail_zip_code, primary_phone, 
EvaluationPriority from """
participant_sql_all = participant_sql+dbSchema+'.CustomerImpactExport' 
table_name = 'participant_customer_account'
pull_write_data(conn, file_dir, participant_sql_all, table_name)

# run query for trade_ally table
trade_ally_sql = """
select * from """
trade_ally_sql_all = trade_ally_sql+dbSchema+'.TradeAlly' 
table_name = 'trade_ally' 
pull_write_data(conn,file_dir,trade_ally_sql_all,table_name)

# run query for participant_impact table and write to csv file 
participant_impact_sql = """
select * from """
participant_impact_sql_all = participant_impact_sql+dbSchema+'.ParticipantImpact' 
table_name='participant_impact'
pull_write_data(conn, file_dir, participant_impact_sql_all, table_name)

# run query for onsite Q1Q2 sample list 
sampleList_sql = """
select * from """
sampleList_sql_all = sampleList_sql+dbSchema+'.onsiteSampleListQ1Q2'
table_name='onsite_sample_list_q1q2'
pull_write_data(conn, file_dir, sampleList_sql_all, table_name)

# run query for desk review Q1Q2 sample list 
sampleList_sql = """
select * from """
sampleList_sql_all = sampleList_sql+dbSchema+'.deskReviewSampleListQ1Q2'
table_name='desk_review_sample_list_q1q2'
pull_write_data(conn, file_dir, sampleList_sql_all, table_name)

print('\nAll tables written to '+file_dir)


    



