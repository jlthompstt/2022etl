# -*- coding: utf-8 -*-
"""
Created on Thu Feb 24 16:28:32 2022

@author: JESSICA.THOMPSON
This script uploads the prepared pyidLookup table to the TX EMV PUCT database. 
Prior to running, use information from trackingDataCheck file to create lookupTableUpdates file.
Prior to running, update loadDataDir, fileName, and dbSchema with values for this PY. 
Updates here are based on Utility+UtilityProgramName+Sector.
"""


#%% set variables used for script 

import pandas as pd 
import pyodbc 
import datetime 
from sqlalchemy import create_engine

programYear = 2022
dbSchema = 'TX2022'
dataDir = "F://Texas PUC 2021-2024/EMV Database/2022etl/data/"
lookupFile = 'AssignProgramYearID.xlsx'

#%% create necessary view used in AssignProgramYearID.xlsx 

# open connection
conn = pyodbc.connect('Driver={SQL Server};Server=etss704sqldevqa,50179;Database=TX_EMV_PUCT;Trusted_Connection=yes;')

cursor = conn.cursor()

sql_statement1 = """
.trackingSummary 
as 
select ParticipantImpactID, ParticipantID, MeasureID, TradeAllyID, ProgramYearID, 
utility as Utility, program_name as UtilityProgramName, participant_code, 
coalesce(sector, market_sector) as Sector, data_provider, esiid,
SourceDataRow, incentive, exante_savings_kw,
exante_savings_kwh, project_id 
from TX_EMV_PUCT.TX2022.CLEAResultTracking
union
select ParticipantImpactID, ParticipantID, MeasureID, TradeAllyID, ProgramYearID, 
utility as Utility, visiondsm_program as UtilityProgramName, participant_code, 
sector as Sector, data_provider, esiid,
SourceDataRow, incentive, exante_savings_kw,
exante_savings_kwh, project_id
from TX_EMV_PUCT.TX2022.centerPointTracking
union
select ParticipantImpactID, ParticipantID, MeasureID, TradeAllyID, ProgramYearID,
utility as Utility, program_name as UtilityProgramName, participant_code, 
sector as Sector, data_provider, esiid,
SourceDataRow, incentive, exante_savings_kw,
exante_savings_kwh, project_id
from TX_EMV_PUCT.TX2022.epeApplianceTracking
union
select ParticipantImpactID, ParticipantID, MeasureID, TradeAllyID, ProgramYearID, 
utility as Utility, program_name as UtilityProgramName, participant_code,  
sector as Sector, data_provider, esiid,
SourceDataRow, incentive, exante_savings_kw,
exante_savings_kwh, project_id
from TX_EMV_PUCT.TX2022.frontierTracking
union
select ParticipantImpactID, ParticipantID, MeasureID, TradeAllyID, ProgramYearID, 
utility as Utility, program_name as UtilityProgramName, participant_code,  
sector as Sector, data_provider, esiid,
SourceDataRow, incentive, exante_savings_kw,
exante_savings_kwh, project_id
from TX_EMV_PUCT.TX2022.oncorTracking
union
select ParticipantImpactID, ParticipantID, MeasureID, TradeAllyID, ProgramYearID, 
utility as Utility, program_name as UtilityProgramName, participant_code,  
sector as Sector, data_provider, 'DUMMY' as esiid,
SourceDataRow, 0 as incentive, CAST([gross_customer_kw_w/isr] as float) as exante_savings_kw,
CAST([gross_customer_kwh_w/isr] as float) as exante_savings_kwh, project_id
from TX_EMV_PUCT.TX2022.xcelHomeLightingTracking
"""

# check if exists 
sql_query = """
select
    count(*)
from
    INFORMATION_SCHEMA.VIEWS
where
    table_name = 'trackingSummary'
    and table_schema = ?
"""

cursor.execute(sql_query, dbSchema)
results = cursor.fetchall()

if results[0][0]>0:
    sql_statement = 'alter view '+dbSchema+sql_statement1
    cursor.execute(sql_statement)
    conn.commit()
else:
    sql_statement = 'create view '+dbSchema+sql_statement1
    cursor.execute(sql_statement)
    conn.commit()


#%% read in lookupTableUpdates info

# go into 'AssignProgramYearID.xlsx' file and make updates to trackingSummary sheet first!

lkpTable = pd.read_excel(dataDir+lookupFile,sheet_name='trackingSummary')

# keep those with new or updated PYID and only relevant columns 
lkpTable = lkpTable.loc[~lkpTable['NewProgramYearID'].isnull(),['NewProgramYearID','Utility',
                                                             'UtilityProgramName','Sector']]

#%% if exists, pull current lookup data 

sqlQuery1 = "If exists (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='"
sqlQuery2 = "' AND TABLE_NAME='pyidLookup') select * from "
sqlQuery3 = ".pyidLookup else select '' as ProgramYearID, '' as Utility, '' as UtilityProgramName, '' as Sector, '' as createdDate;"
sqlQuery = sqlQuery1+dbSchema+sqlQuery2+dbSchema+sqlQuery3

existingLookup = pd.read_sql(sqlQuery, conn)

#%% sort lkpTable entries into updates and inserts 

lookupExisting = lkpTable.merge(
    existingLookup[['Utility','UtilityProgramName','Sector','createdDate']],
    on=['Utility','UtilityProgramName','Sector'],how='left')

# inserts 
lookupExistingInserts = lookupExisting.loc[lookupExisting['createdDate'].isnull(),]
lookupExistingInserts['createdDate'] = datetime.datetime.now()
lookupExistingInserts['modifiedDate'] = datetime.datetime.now()
lookupExistingInserts.rename(columns = {'NewProgramYearID':'ProgramYearID'}, inplace=True)
print('prepared '+str(len(lookupExistingInserts))+' records for insertion')

# updates 
lookupExistingUpdates = lookupExisting.loc[~lookupExisting['createdDate'].isnull(),]
lookupExistingUpdates['modifiedDate'] = datetime.datetime.now()
print('prepared '+str(len(lookupExistingUpdates))+' records for update')

#%% write updates and inserts to database 

# create sqlalchemy engine
con_str = 'mssql+pyodbc://etss704sqldevqa:50179/'+'TX_EMV_PUCT'+'?trusted_connection=yes&driver=SQL Server Native Client 11.0'
engine = create_engine(con_str, fast_executemany=True)

# inserts 
if len(lookupExistingInserts)>0:
    print(datetime.datetime.now())
    lookupExistingInserts.to_sql('pyidLookup', con = engine, index=False, schema=dbSchema, if_exists='append')
    print(datetime.datetime.now())
    print(str(len(lookupExistingInserts))+' rows inserted to '+dbSchema+'.pyidLookup')
    conn.commit()

if len(lookupExistingUpdates)>0:
    lookupExistingUpdates.to_sql('tempUpdates', con = engine, index=False, schema=dbSchema, if_exists='replace')
    conn.commit()
    # run merge statement 
    sqlStatement = """
    .tempUpdates as source 
    on source.Utility=target.Utility 
    and source.UtilityProgramName=target.UtilityProgramName 
    and source.Sector=target.Sector 
    when matched then update set  
    target.ProgramYearID=source.NewProgramYearID, 
    target.modifiedDate=source.modifiedDate;
    """
    cursor.execute('merge '+dbSchema+'.pyidLookup as target using '+dbSchema+sqlStatement)
    conn.commit()
    cursor.execute('drop table '+dbSchema+'.tempUpdates;')
    conn.commit()
    print(str(len(lookupExistingUpdates))+' rows updated in '+dbSchema+'.pyidLookup')
    conn.commit()


#%% pull pyidlookup table data and compare to tracking table programs 

sqlQuery = """
select distinct Utility, UtilityProgramName, Sector 
from TX2022.trackingSummary
"""

currentTracking = pd.read_sql(sqlQuery,conn)

sqlQuery1 = "If exists (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='"
sqlQuery2 = "' AND TABLE_NAME='pyidLookup') select * from "
sqlQuery3 = ".pyidLookup else select '' as ProgramYearID, '' as Utility, '' as UtilityProgramName, '' as Sector, '' as createdDate;"
sqlQuery = sqlQuery1+dbSchema+sqlQuery2+dbSchema+sqlQuery3

existingLookup = pd.read_sql(sqlQuery, conn)

unmatchedLookup = currentTracking.merge(existingLookup,how='outer',indicator=True)

print('Programs in tracking data but missing from Program Year ID lookup')
print(unmatchedLookup.loc[unmatchedLookup['_merge']=='left_only',])

#%% update ProgramYearID in tracking tables 

sqlQuery = """
.pyidLookup as source 
on source.Utility=target.utility 
and source.UtilityProgramName=target.program_name 
and source.Sector=target.sector 
when matched then update 
set target.ProgramYearID=source.ProgramYearID;
"""

trackingTables = ['epeApplianceTracking',
                  'frontierTracking','oncorTracking','xcelHomeLightingTracking']

for trackTable in trackingTables:
    cursor.execute('merge '+dbSchema+'.'+trackTable+' as target using '+dbSchema+sqlQuery)
    conn.commit()

# CenterPoint and CLEAResult are weird until we fix columns 
sqlQuery = """
.pyidLookup as source 
on source.Utility=target.utility 
and source.UtilityProgramName=target.program_name 
and source.Sector=coalesce(target.sector,target.market_sector)
when matched then update 
set target.ProgramYearID=source.ProgramYearID;
"""

trackingTables = ['CLEAResultTracking']

for trackTable in trackingTables:
    cursor.execute('merge '+dbSchema+'.'+trackTable+' as target using '+dbSchema+sqlQuery)
    conn.commit()
    
sqlQuery = """
.pyidLookup as source 
on source.Utility=target.utility 
and source.UtilityProgramName=target.visiondsm_program 
and source.Sector=target.sector 
when matched then update 
set target.ProgramYearID=source.ProgramYearID;
"""

trackingTables = ['centerPointTracking']

for trackTable in trackingTables:
    cursor.execute('merge '+dbSchema+'.'+trackTable+' as target using '+dbSchema+sqlQuery)
    conn.commit()



#%% output summary of tracking data matched to EEPRs programs 

sqlQuery = """
select t.ProgramYearID as trackingPYID, t.Utility trackingUtility, 
t.UtilityProgramName trackingUtilityProgramName, t.Sector, 
t.data_provider, t.cntParts, t.cntRecords, t.sumIncentive, t.sumExAnte_Savings_kW, 
t.sumExAnte_Savings_kWh, py.ProgramYearID, py.UtilityName, py.UtilityProgramName, 
py.ProgramName, py.SectorName, 
py.ReportSectorDescription, py.ProgramGroupName, py.ProgramTypeName 
from (select ProgramYearID, Utility, UtilityProgramName, Sector, data_provider, 
      count(distinct esiid) as cntParts, count(*) as cntRecords, 
      sum(incentive) as sumIncentive, sum(exante_savings_kw) as sumExAnte_Savings_kW, 
      sum(exante_savings_kwh) as sumExAnte_Savings_kWh from TX2022.trackingSummary 
      group by ProgramYearID, Utility, UtilityProgramName, Sector, data_provider) t 
full outer join TX2022.ProgramYearList py
on t.ProgramYearID=py.ProgramYearID 
"""

programMatchInfo = pd.read_sql(sqlQuery, conn)

programMatchInfo.to_csv(dataDir+'trackingEEPRmatch.csv',index=False,
                        line_terminator='\n')


