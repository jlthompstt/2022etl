# -*- coding: utf-8 -*-
"""
Created on Wed Mar 16 11:13:10 2022

@author: JESSICA.THOMPSON
This script scapes table data out of EEPRs pdf reports. 
Before running, save most recent EEPRs pdf files to 'current' folders in each utility EEPRs folder
Tables are scraped and saved to tabs in an excel workbook, two workbooks per utility
Use the version 1 workbook first. For tables that don't save correctly in version 1 - 
usually tables w/o distinct gridlines - use version 2
If you get a ghostscript error, try restarting your kernel
"""

#%% set variables used for script 

import pandas as pd
import camelot
from os import walk

programYear = 2022
dataDir = "F://Texas PUC 2021-2024/Client Information/EEPR/"

#%% create list of files to load

# generate list of files in dataDir 
# end result is table of utility, file_name
dirs = next(walk(dataDir+str(programYear)+'/'), (None, None, []))[1]
# remove any unwanted directories
# 20220404: Chuck reducing dirs to just El Paso to try to trouble shoot why that PDF doesn't work
dirs = ['El Paso Electric']

f = []
for x in dirs:
    dirStr = dataDir+str(programYear)+'/'+x+'/current/'
    files = next(walk(dirStr), (None, None, []))[2]
    for z in files: 
        list1 = [x,z.lower(),dirStr]
        f.append(list1)

fileList = pd.DataFrame(f,columns=['utility','file_name','dirStr'])
# keep only .pdf files and remove xlsx files
fileList = fileList[fileList['file_name'].str.contains('.pdf')].reset_index(drop=True)
fileList = fileList[~fileList['file_name'].str.contains('.xlsx')].reset_index(drop=True)

# remove any other extraneous files if necessary by putting their file_name in the list 
removeFiles = []
#removeFiles = ['2022 epe eepr final.pdf']

fileList = fileList.loc[~fileList['file_name'].isin(removeFiles),].reset_index(drop=True)

#%% scrape tables from pdf files and write them to excel file (one per utility)

for i in range(len(fileList)):
    utility = fileList.loc[i,'utility']
    fileName = fileList.loc[i,'file_name']
    dirStr = fileList.loc[i,'dirStr']
    
    ## Using flavor= stream
    tableList1 = camelot.read_pdf(dirStr+fileName,pages='all',flavor='stream',strip_text='\n')
    writer = pd.ExcelWriter(dirStr+utility+fileName[0:-4]+'EEPRtablesv2.xlsx', engine='xlsxwriter', 
                            options={'strings_to_formulas': False, 
                                     'strings_to_numbers': True,
                                     'strings_to_urls': False})
    for j in range(len(tableList1)):
        tableList1[j].df.to_excel(writer, sheet_name='table'+str(j), index=False)
    writer.save()
    print('*'*10 + utility + " file 2 done.")
    
    ## Using flavor=lattice (default)
    tableList2 = camelot.read_pdf(dirStr+fileName,pages='all', strip_text='\n')
    writer = pd.ExcelWriter(dirStr+utility+fileName[0:-4]+'EEPRtablesv1.xlsx', engine='xlsxwriter', 
                            options={'strings_to_formulas': False, 
                                     'strings_to_numbers': True,
                                     'strings_to_urls': False})
    for j in range(len(tableList2)):
        tableList2[j].df.to_excel(writer, sheet_name='table'+str(j), index=False)
    writer.save()
    print('*'*10 + utility + " file 1 done.")

print("All done.")



