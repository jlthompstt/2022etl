# -*- coding: utf-8 -*-
"""
Created on Tue Sep 14 11:23:12 2021

@author: JESSICA.THOMPSON
after all final data, load Najoua's final load management numbers 
"""

#%% set data directory and variables

import pandas as pd 
import pyodbc 
from sqlalchemy import create_engine
import datetime

loadDataDir = "F://Texas PUC 2021-2024/Client Information/PY2021 Data Requests/14 2021 Final Tracking Data Request/forLoad/data/"
fileName = 'loadManagement2021.csv'
programYear = 2021

#%% load in data file and list programs included 

lmData = pd.read_csv(loadDataDir+fileName)


print(lmData[['Utility','Program','Sector']].drop_duplicates())
programs = lmData[['Utility','Program','Sector']].drop_duplicates()

#%% write data to temporary table 
# open connection
conn = pyodbc.connect('Driver={SQL Server};Server=ETSS704SQLDEVQA,50179;Database=EMV_PUCT_staging;Trusted_Connection=yes;')
cursor = conn.cursor()

# create sqlalchemy engine
con_str = 'mssql+pyodbc://ETSS704SQLDEVQA:50179/'+'EMV_PUCT_staging'+'?trusted_connection=yes&driver=SQL Server Native Client 11.0'
engine = create_engine(con_str, fast_executemany=True)
print(datetime.datetime.now())
lmData.to_sql('LM2021', con = engine, index=False, schema='TrackingData',if_exists='replace')
print(datetime.datetime.now())
print(str(len(lmData))+' rows inserted to LM2021')
conn.commit()

#%% run customer account encryption routine

tableName = 'LM2021'
schemaName = 'TrackingData'
accountFieldName = 'ProgramYearID'

cursor.execute("EXEC EncryptAndStoreCustomerAccounts @TableName=?, @SchemaName=?, @AccountFieldName=?;",tableName, schemaName, accountFieldName)
conn.commit()
cursor.execute("EXEC AddCleanKeysToTable @TableName=?, @SchemaName=?, @AccountFieldName=?;",tableName, schemaName, accountFieldName)
conn.commit()

#%% insert customer data

sqlQuery = """
insert into Customer (CustomerAccountID,CustomerName) 
select distinct CustomerAccountID, 
'Dummy Customer '+CAST(ProgramYearID as char) as CustomerName 
from TrackingData.LM2021 
where CustomerAccountID not in (
    select c.CustomerAccountID from Customer c join TrackingData.LM2021 a 
    on c.CustomerAccountID=a.CustomerAccountID)
"""
cursor.execute(sqlQuery)
conn.commit()

#%% insert measure data

sqlQuery = """
insert into Measure (SectorID,ProgramYearID,MeasureCategoryID,MeasureDescription,
                     MeasureReportingDescription,QuantityUnitID,SourceMeasureCode,
                     EUL) 
select distinct SectorID, ProgramYearID, 14 as MeasureCategoryID, 'Load Management', 
'Load Management' as MeasureReportingDescription, 1 as QuantityUnitID, 
'Load Management' as SourceMeasureCode, 1 as EUL 
from TrackingData.LM2021 t 
where not exists (select m.SectorID, m.ProgramYearID, m.SourceMeasureCode, m.EUL from Measure m 
where t.SectorID=m.SectorID and t.ProgramYearID=m.ProgramYearID)
"""

cursor.execute(sqlQuery)
conn.commit()

#%% insert participant data

sqlQuery = """
select distinct cast(a.ProgramYearID as varchar)+'-'+cast(b.MeasureID as varchar) as ParticipantCode, 
'ProgramYearID-MeasureID' as ParticipantCodeSource, a.ProgramYearID, c.CustomerID, 
1 as TradeAllyID, 1 as ImportPackageID, convert(bit,1) as IsActive 
from TrackingData.LM2021 a 
join Measure b on a.ProgramYearID=b.ProgramYearID
join Customer c on a.CustomerAccountID=c.CustomerAccountID
"""
participantData = pd.read_sql(sqlQuery,conn)

for index, row in participantData.iterrows():
    cursor.execute("EXEC ParticipantWithCustomerInsert @ParticipantCode=?, @ParticipantCodeSource=?, @ProgramYearID=?, @CustomerID=?, @TradeAllyID=?, @ImportPackageID=?;", row.ParticipantCode,row.ParticipantCodeSource,row.ProgramYearID,row.CustomerID,row.TradeAllyID,row.ImportPackageID)
conn.commit()

#%% insert participant impact data

sqlQuery = """
insert into ParticipantImpact (SourceDataRowID,ParticipantID,MeasureID,
                               ParticipantImpactDate,Count,Quantity,Incentive,
                               ExAnteSavings_kW,ExAnteSavings_kWh,
                               ExPostSavings_kW,ExPostSavings_kWh) 
select 1 as SourceDataRowID, p.ParticipantID, m.MeasureID, 
cast('01/01/2021' as date) as ParticipantImpactDate, 
1 as Count, 1 as Quantity, 
0 as Incentive, sum([ExAnteSavings_kW]) as ExAnteSavings_kW, 
sum([ExAnteSavings_kWh]) as ExAnteSavings_kWh, sum(ExPostSavings_kW) as ExPostSavings_kW, 
sum(ExPostSavings_kWh) as ExPostSavings_kWh from TrackingData.LM2021 t 
join Measure m on t.ProgramYearID=m.ProgramYearID 
join Participant p on t.ProgramYearID=p.ProgramYearID 
where not exists (select pi.ParticipantID, pi.MeasureID  
                  from ParticipantImpact pi 
                  where p.ParticipantID=pi.ParticipantID 
                  and m.MeasureID=pi.MeasureID ) 
group by ParticipantID, MeasureID
"""
cursor.execute(sqlQuery)
conn.commit()
