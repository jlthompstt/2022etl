# -*- coding: utf-8 -*-
"""
Created on Wed Jul  6 12:23:07 2022

@author: JESSICA.THOMPSON
This script updates specific measures in the database. 
Updates EUL and measure category where new values have been assigned  
"""

#%% Setting variables for run and importing packages 

import pyodbc 
import pandas as pd
from sqlalchemy import create_engine

# data dir
dataDir = "F://Texas PUC 2021-2024/EMV Database/2022etl/data/"
# filename 
fileName = 'MeasureCategories.xlsx'
dbSchema = 'TX2022'
database = 'TX_EMV_PUCT'

conn = pyodbc.connect('''Driver={SQL Server};
                      Server=etss704sqldevqa\ETSS704SQL12_QA;
                      Database=TX_EMV_PUCT;
                      Trusted_Connection=yes;''')
cursor = conn.cursor()
con_str = 'mssql+pyodbc://etss704sqldevqa\ETSS704SQL12_QA/' + 'TX_EMV_PUCT' + '?trusted_connection=yes&driver=SQL Server Native Client 11.0'
engine = create_engine(con_str, fast_executemany=True)

#%% pull in data and load measure category updates 

measureData = pd.read_excel(dataDir+fileName,sheet_name='MeasureCategoryUpdates')

# keep rows with updated measure catogories 
measureDataSub = measureData.loc[~measureData['UpdatedMeasureCategoryID'].isnull(),
                              ['measure_id','UpdatedMeasureCategoryID']]
# if measureData has data
if len(measureDataSub)>0:
    # write to a temp table 
    measureDataSub.to_sql('measureData', con = engine, index=False, schema=dbSchema,if_exists='replace')
    conn.commit()
    
    # update table based on temp table 
    queryText = """ 
    on source.measure_id=target.measure_id 
    when matched then update 
    set target.measure_category_id=source.UpdatedMeasureCategoryID;
    """
    cursor.execute('merge '+dbSchema+'.Measure as target using '+dbSchema+'.measureData as source'+queryText)
    conn.commit()

    # drop temp table 
    cursor.execute('drop table '+dbSchema+'.measureData')
    conn.commit()
    print('updated '+str(len(measureDataSub))+' records')
else:
    print('no updates needed')
    
#%% pull in data and eul updates 

# keep rows with updated measure catogories 
measureDataSub = measureData.loc[~measureData['UpdatedEUL'].isnull(),
                              ['measure_id','UpdatedEUL']]
# if measureData has data
if len(measureDataSub)>0:
    # write to a temp table 
    measureDataSub.to_sql('measureData', con = engine, index=False, schema=dbSchema,if_exists='replace')
    conn.commit()
    
    # update table based on temp table 
    queryText = """ 
    on source.measure_id=target.measure_id 
    when matched then update 
    set target.eul=source.UpdatedEUL;
    """
    cursor.execute('merge '+dbSchema+'.Measure as target using '+dbSchema+'.measureData as source'+queryText)
    conn.commit()

    # drop temp table 
    cursor.execute('drop table '+dbSchema+'.measureData')
    conn.commit()
    print('updated '+str(len(measureDataSub))+' records')
else:
    print('no updates needed')

#%% pull updated data from db to double-check

sql_query = """
select a.measure_id, a.measure_reporting_description, 
mc.MeasureCategoryName, mc.MeasureCategoryID, a.eul
from """
sqlQuery2 = """.Measure a 
join lookup.MeasureCategory mc on a.measure_category_id=mc.MeasureCategoryID 
"""

checkData = pd.read_sql(sql_query+dbSchema+sqlQuery2, conn)
dataCheck = measureData.loc[(~measureData['UpdatedMeasureCategoryID'].isnull()) | 
                            (~measureData['UpdatedEUL'].isnull()),
                            ['UtilityName','program_year_id','measure_id',
                             'measure_description','UpdatedMeasureCategoryID',
                             'UpdatedEUL']].merge(checkData,on=['measure_id'],how='left')


