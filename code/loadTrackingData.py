# -*- coding: utf-8 -*-
"""
Created on Fri Feb 18 12:17:01 2022

@author: JESSICA.THOMPSON 
This script is used to catalog tracking data received and load it to the tracking tables in the db. 
Before running, go through and open all excel files to be loaded and save the relevant sheet as csv.
Remove totals and any extra rows at top before saving as csv file, also split any combined cells.
"""
# %% set variables used for script

from pickle import TRUE
from re import T
from sys import displayhook
from distutils import filelist
import pandas as pd
import pyodbc
import numpy as np
from datetime import datetime
from sqlalchemy import create_engine
import os 
from os import walk
from pathlib import Path
import glob
import re 

programYear = 2022
schema = 'TX2022'
database = 'TX_EMV_PUCT'
dataDir = "F://Texas PUC 2021-2024/Client Information/PY2022 Data Requests/02 PY2022 Q1-Q2 Tracking Data/"
trackingFile = 'TX data request PY2022 Q1-Q4 Prelimary Data- status.xlsx'
xwalkDirectory = "F://Texas PUC 2021-2024/EMV Database/2022etl/data/"
conn = pyodbc.connect('''Driver={SQL Server};
                      Server=etss704sqldevqa\ETSS704SQL12_QA;
                      Database=TX_EMV_PUCT;
                      Trusted_Connection=yes;''')

cursor = conn.cursor()
con_str = 'mssql+pyodbc://etss704sqldevqa\ETSS704SQL12_QA/' + 'TX_EMV_PUCT' + '?trusted_connection=yes&driver=SQL Server Native Client 11.0'
engine = create_engine(con_str, fast_executemany=True)

   

# %% create functions used in script


def load_data(data_provider):
    """
    Concatenates files into one dataframe. 
    Can only be used for files w/ identical columns. 
    Parameters 
    ----------

    data_provider : Utility name (string or string variable)

    Returns
    -------
     df2: Concatenated dataframe of all files read
   

    """
 
    # Empty dataframe list 
    df = []
    # Recusrive glob to iterate through the directory according to directory and file type
    fileList = Path(dataDir + "\\" + data_provider).rglob("*.csv")
    # create sub_dir, file_name from the paths from glob and appends each file as a data frame to df 
    for files in fileList:
        sub_dir = os.path.split(os.path.split(files)[0])[1]
        data_provider = data_provider
        file_name = os.path.basename(files)
        sub_dirdatetime = datetime.now()
        csvdf = pd.read_csv(files, low_memory=False, dtype=str)
        csvdf.columns = csvdf.columns.str.replace('\n', '')
        csvdf.columns = csvdf.columns.str.strip()
        csvdf.columns = csvdf.columns.str.replace(' ','_')
        csvdf = csvdf.rename(columns=str.lower)
        if 'Residential' in str(files):
            csvdf["sector"] = 'Residential'
        elif 'Commercial' in str(files):
            csvdf["sector"] = 'Commercial'
        else:
            pass
        csvdf["sub_dir"] = sub_dir
        csvdf["data_provider"] = data_provider
        csvdf["file_name"] = file_name
        df.append(csvdf)
    
   # Concats the data files above
    df2 = pd.concat(df)
    df2.columns = df2.columns.str.replace('\n', '')
    df2.columns = df2.columns.str.strip()
    df2.columns = df2.columns.str.replace(' ','_')
    df2 = df2.rename(columns=str.lower)
    df2 = df2.fillna(np.nan)
    df = df2.dropna(axis=1, how='all')
    print((sub_dirdatetime))
    return df2
    


def pullTrackingData(table, dbSchema):
    """
    Pulls in data from table if existing, otherwise returns 0.
    Parameters
    ----------
    table : tableName
    dbSchema: db schema name
    

    Returns
    5
    -------
    tableData: df of returned data or 0 if tables doesn't exist

    """

    sqlQuery1 = "If exists (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='"
    sqlQuery2 = "' AND TABLE_NAME='"
    sqlQuery2b = "') select * from "
    sqlQuery3 = " else select 0;"
    sqlQuery = sqlQuery1 + dbSchema + sqlQuery2 + table + sqlQuery2b + dbSchema + '.' + table + sqlQuery3

    tableData = pd.read_sql(sqlQuery, conn)
    tableData = tableData.fillna(np.nan)
    return (tableData)


def insertAndUpdate(tableName, schema, uniqueColumns, dbTable, sourceDf):
    """
    This function uses inputs to sort newly received data into updates and inserts, 
    then loads these into the database. 

    Parameters
    ----------
    tableName : string, name of tracking data table
    schema : string, name of tracking schema
    uniqueColumns : list, list of columns in natural key for this source
    dbTable : df, dataframe pulled from db
    sourceDf : df, dataframe loaded from received data

    Returns
    -------
    (insertCnt, updateCnt): count of inserts and updates processed

    """
    # if there is existing data in the db
    print(datetime.now())
    if dbTable.size > 1 and len(uniqueColumns) >= 1:
        # set columns to keep from db table in join
        colsKeep = uniqueColumns + ['created_date', 'SourceDataRow']
        colNamesSource = sourceDf.columns.tolist()
        colNamesDb = dbTable.columns.tolist()
        # set columns to exclude from compare
        colsExclude = colsKeep + ['modified_date', 'ParticipantImpactID', 'ParticipantID', 'MeasureID', 'TradeAllyID',
                                  'sub_dir', 'file_name', 'data_provider']
        colHashSource = list(set(colNamesSource) - set(colsExclude))
        colHashDb = list(set(colNamesDb) - set(colsExclude))
        # create hash for comparison for db data and source data 
        dbTable['chksum_db'] = dbTable[colHashDb].apply(lambda x: hash(tuple(x)), axis=1)
        sourceDf['chksum'] = sourceDf[colHashSource].apply(lambda x: hash(tuple(x)), axis=1)
        colsKeep = colsKeep + ['chksum_db']
        # join db data to source data on unique columns 
        dbSourceMerge = sourceDf.merge(
            dbTable[colsKeep],
            on=uniqueColumns, how='left')
        # separate inserts and write them
        inserts = dbSourceMerge.loc[dbSourceMerge['created_date'].isnull(),].reset_index(drop=True)
        if len(inserts) > 0:
            inserts['created_date'] = datetime.now()
            inserts['modified_date'] = datetime.now()
            # remove extra columns 
            inserts = inserts.drop(columns=['chksum', 'chksum_db', 'SourceDataRow'])
            inserts.to_sql(tableName, con=engine, index=False, schema=schema, if_exists='append')
            print('inserted ' + str(len(inserts)) + ' records')
            conn.commit()
        # separate updates, then first delete, then insert them
        updates = dbSourceMerge.loc[(~dbSourceMerge['created_date'].isnull()) &
                                    (dbSourceMerge['chksum'] != dbSourceMerge['chksum_db']),].reset_index(drop=True)
        if len(updates) > 0:
            updates['modified_date'] = datetime.now()
            delIDs = updates[['SourceDataRow']]
            delIDs.to_sql('tempDeletes', con=engine, index=False, schema=schema, if_exists='replace')
            queryText = 'delete from ' + schema + '.' + tableName + ' where SourceDataRow in (select SourceDataRow from ' + schema + '.tempDeletes);'
            cursor.execute(queryText)
            conn.commit()
            cursor.execute('drop table ' + schema + '.tempDeletes;')
            conn.commit()
            print('deleted ' + str(len(updates)) + ' records')
            # remove extra columns 
            updates = updates.drop(columns=['chksum', 'chksum_db', 'SourceDataRow'])
            updates.to_sql(tableName, con=engine, index=False, schema=schema, if_exists='append')
            print('inserted ' + str(len(updates)) + ' updated records')
        conn.commit()
        updateCnt = str(len(updates))
        insertCnt = str(len(inserts))
    else:
        inserts = sourceDf.copy()
        inserts['created_date'] = datetime.now()
        inserts['modified_date'] = datetime.now()
        inserts.to_sql(tableName, con=engine, index=False, schema=schema, if_exists='append')
        print(str(len(inserts)) + ' rows inserted to ' + schema + '.' + tableName)
        queryText = 'alter table ' + schema + '.' + tableName
        cursor.execute(
            queryText + " add ProgramYearID int null, ParticipantImpactID int null, ParticipantID int null, MeasureID int null, TradeAllyID int null, SourceDataRow int not null identity primary key;")
        conn.commit()
        updateCnt = '0'
        insertCnt = str(len(inserts))
    return insertCnt, updateCnt


def truncateAndInsert(tableName, schema, uniqueColumns, dbTable, sourceDf):
    """
    This function truncates the tracking table if it exists and then inserts 
    source data into the database. 

    Parameters
    ----------
    tableName : string, name of tracking data table
    schema : string, name of tracking schema
    uniqueColumns : list, list of columns in natural key for this source
    dbTable : df, dataframe pulled from db
    sourceDf : df, dataframe loaded from received data

    Returns
    -------
    insertCnt : count of inserts processed

    """
    # if there is existing data in the db
    print(datetime.now())
    if dbTable.size > 1:
        # drop existing table 
        query_text = 'drop table ' + schema + '.' + tableName + ';'
        cursor.execute(query_text)
        conn.commit()
        print('dropped table')
    # insert source data to new table 
    sourceDf['created_date'] = datetime.now()
    sourceDf['modified_date'] = datetime.now()
    sourceDf.to_sql(tableName, con=engine, index=False, schema=schema, if_exists='append')
    print(str(len(sourceDf)) + ' rows inserted to ' + schema + '.' + tableName)
    queryText = 'alter table ' + schema + '.' + tableName
    cursor.execute(
        queryText + " add ProgramYearID int null, ParticipantImpactID int null, ParticipantID int null, MeasureID int null, TradeAllyID int null, SourceDataRow int not null identity primary key;")
    conn.commit()
    insertCnt = str(len(sourceDf))
    return insertCnt


def harmonizeColumns(tableName, sourceDf, xwalkDf):
    """
    This function adds harmonized columns to source data based on logic in the crosswalk table.

    Parameters
    ----------
    tableName : string, name of tracking table
    sourceDf : df, dataframe of source data
    xwalkDf : df, crosswalk dataframe

    Returns
    -------
    updatedSource: df, sourceDf with added columns

    """
    xwalkSub = xwalkDf.loc[xwalkDf['TrackingTable'] == tableName,]
    colsDf = pd.DataFrame(sourceDf.columns.tolist(), columns=['TrackingField'])
    xwalkMerge = xwalkSub.merge(colsDf, how='left')
    # create columns 
    updatedSource = sourceDf.copy()
    for i in range(len(xwalkMerge)):
        newCol = xwalkMerge.loc[i, 'mainField']
        oldCol = xwalkMerge.loc[i, 'TrackingField']
        construct1 = xwalkMerge.loc[i, 'construct1']
        construct2 = xwalkMerge.loc[i, 'construct2']
        typ = xwalkMerge.loc[i, 'type']
        default = xwalkMerge.loc[i, 'default']
        if pd.notna(typ):
            if typ == 'mask':
                if pd.notna(construct2):
                    updatedSource[newCol] = updatedSource[construct1].mask(pd.isnull, updatedSource[construct2])
                else:
                    updatedSource[newCol] = updatedSource[construct1].mask(pd.isnull, default)
            if typ == 'concat':
                updatedSource[newCol] = updatedSource[construct1].mask(pd.isnull, '') + ' ' + updatedSource[construct2].mask(pd.isnull, '')
        elif pd.notna(oldCol):
            updatedSource[newCol] = updatedSource[oldCol]
        else:
            updatedSource[newCol] = default
    # convert desired columns
    if 'exante_savings_kw' in updatedSource: 
        updatedSource['exante_savings_kw'] = updatedSource['exante_savings_kw'].apply(
            lambda x: x.replace(',', '') if isinstance(x, str) else x)
        updatedSource['exante_savings_kw'] = updatedSource['exante_savings_kw'].apply(
            lambda x: x.replace('(', '-') if isinstance(x, str) else x)
        updatedSource['exante_savings_kw'] = updatedSource['exante_savings_kw'].apply(
            lambda x: x.replace(')', '') if isinstance(x, str) else x)
        updatedSource['exante_savings_kw'] = updatedSource['exante_savings_kw'].apply(
            lambda x: x.replace('$', '') if isinstance(x, str) else x)
        updatedSource['exante_savings_kwh'] = updatedSource['exante_savings_kwh'].apply(
            lambda x: x.replace(',', '') if isinstance(x, str) else x)
        updatedSource['exante_savings_kwh'] = updatedSource['exante_savings_kwh'].apply(
            lambda x: x.replace('(', '-') if isinstance(x, str) else x)
        updatedSource['exante_savings_kwh'] = updatedSource['exante_savings_kwh'].apply(
            lambda x: x.replace(')', '') if isinstance(x, str) else x)
        updatedSource['exante_savings_kwh'] = updatedSource['exante_savings_kwh'].apply(
            lambda x: x.replace('$', '') if isinstance(x, str) else x)
        updatedSource['exante_savings_kw'] = pd.to_numeric(updatedSource['exante_savings_kw'], errors='coerce')
        updatedSource['exante_savings_kwh'] = pd.to_numeric(updatedSource['exante_savings_kwh'], errors='coerce')
    if'incentive' in updatedSource:
        updatedSource['incentive'] = updatedSource['incentive'].apply(
            lambda x: x.replace(',', '') if isinstance(x, str) else x)
        updatedSource['incentive'] = updatedSource['incentive'].apply(
            lambda x: x.replace('(', '-') if isinstance(x, str) else x)
        updatedSource['incentive'] = updatedSource['incentive'].apply(
            lambda x: x.replace(')', '') if isinstance(x, str) else x)
        updatedSource['incentive'] = updatedSource['incentive'].apply(
            lambda x: x.replace('$', '') if isinstance(x, str) else x)
        updatedSource['incentive'] = pd.to_numeric(updatedSource['incentive'], errors='coerce')
    if 'eul' in updatedSource:   
        updatedSource['eul'] = pd.to_numeric(updatedSource['eul'], errors='coerce')
    if 'participant_impact_date' in updatedSource:
        updatedSource['participant_impact_date'] = pd.to_datetime(updatedSource['participant_impact_date'])
    return updatedSource




# %% load crosswalk table of tracking data columns mapped to harmonized columns

xwalk = pd.read_excel(xwalkDirectory + 'fieldLookup.xlsx')

# %% load frontier data and check


# creates dataframe using load_data function
frontierDataDf = load_data("Frontier")

# remove extra rows 
frontierDataDf = frontierDataDf.loc[~frontierDataDf['utility'].isnull(),]
frontierDataDf = frontierDataDf.loc[~frontierDataDf['program_name'].isnull(),]
# fix Utility names
frontierDataDf.loc[frontierDataDf['utility'].str.contains('TCC', case=False) |
                   frontierDataDf['utility'].str.contains('TNC', case=False),
                   'utility'] = 'AEP Texas'
frontierDataDf.loc[frontierDataDf['utility'].str.contains('Swepco', case=False),
                   'utility'] = 'SWEPCO'

# check for any with missing Sector
missingSector = frontierDataDf.loc[frontierDataDf['sector'].isnull(),
                                   ['sector', 'program_name']].drop_duplicates()
if len(missingSector) > 0:
    print('These programs need a Sector assigned')
    print(missingSector)

# create harmonized columns using loaded crosswalk table
tableName = 'frontierTracking'
frontierDataDf = harmonizeColumns(tableName, frontierDataDf, xwalk)

uniqueColumns = ['utility', 'program_name', 'sector', 'measure_id']
recordCount = len(frontierDataDf)
recordCountU = len(frontierDataDf[uniqueColumns].drop_duplicates())
if recordCount > recordCountU:
    print("Double-check your list of unique columns, it seems to be incomplete.")

# if db table exists, check to be sure all source columns are present 
frontierDb = pullTrackingData(tableName, schema)
colNamesDb = frontierDb.columns.tolist()
if len(colNamesDb) > 1:
    colNamesSource = frontierDataDf.columns.tolist()
    extraCols = list(set(colNamesSource) - set(colNamesDb))
    if len(extraCols) > 0:
        print('There are columns present in source data but not database table.')
        print(extraCols)
        print('You will need to add these columns to the database table or drop the table and start over.')

# %% if no issues, load frontier data

# run function to insert and update records 
insertCnt, updateCnt = insertAndUpdate(tableName, schema, uniqueColumns, frontierDb, frontierDataDf)
print("Wrote " + insertCnt + " inserts to " + database + '.' + schema + '.' + tableName)
print("Wrote " + updateCnt + " updates to " + database + '.' + schema + '.' + tableName)

# %% load CLEAResult data and check

# creates dataframe using load_data function
crDataDf = load_data("CLEAResult")

# remove empty extra rows 
crDataDf = crDataDf.loc[~crDataDf['utility_client'].isnull(),]
crDataDf = crDataDf.loc[~crDataDf['program_name'].isnull(),]
# fix Utility names

variable1 = ['AEP','El Paso', 'Entergy', 'Southwestern','New Mexico']
variable2 = ['AEP Texas','El Paso Electric', 'Entergy', 'SWEPCO','TNMP']
for x1,x2 in zip(variable1,variable2):
    crDataDf.loc[crDataDf['utility_client'].str.contains(x1, case=False),
             'utility_client'] = x2

# create Sector where missing
vars1 = ['Commercial','Business','SCORE'] 
vars2 = ['Residential']
for v1,v2 in zip(vars1,vars2):
    crDataDf.loc[crDataDf['market_sector'].isnull() &
                crDataDf['program_name'].str.contains(v1, case=False),
                'market_sector'] = 'Commercial'
    crDataDf.loc[crDataDf['market_sector'].isnull() &
                crDataDf['program_name'].str.contains(v2, case=False),
                'market_sector'] = 'Residential'

crDataDf.loc[crDataDf['market_sector'].isnull() &
             crDataDf['file_name'].str.contains('Comm', case=False),
             'market_sector'] = 'Commercial'
crDataDf.loc[crDataDf['market_sector'].isnull() &
             crDataDf['file_name'].str.contains('Res', case=False),
             'market_sector'] = 'Residential'
# fix program with Sector incorrectly set to SCORE 
crDataDf.loc[crDataDf['market_sector'] == 'SCORE', 'market_sector'] = 'Commercial'
# fix sector using program_name
crDataDf.loc[crDataDf['program_name'].str.contains('SCORE'), 'market_sector'] = 'Commercial'
crDataDf.loc[crDataDf['program_name'].str.contains('Business'), 'market_sector'] = 'Commercial'

# check for any with missing Sector
missingSector = crDataDf.loc[crDataDf['market_sector'].isnull(),['market_sector', 'program_name']].drop_duplicates()
if len(missingSector) > 0:
    print('These programs need a Sector assigned')
    print(missingSector)

# create harmonized columns using loaded crosswalk table
tableName = 'CLEAResultTracking'
crDataDf = harmonizeColumns(tableName, crDataDf, xwalk)

uniqueColumns = ['utility_client', 'program_name', 'measure_calculation_id']
recordCount = len(crDataDf)
recordCountU = len(crDataDf[uniqueColumns].drop_duplicates())
if recordCount > recordCountU:
    print("Double-check your list of unique columns, it seems to be incomplete.")

# if db table exists, check to be sure all source columns are present 
crDb = pullTrackingData(tableName, schema)
colNamesDb = crDb.columns.tolist()
if len(colNamesDb) > 1:
    colNamesSource = crDataDf.columns.tolist()
    extraCols = list(set(colNamesSource) - set(colNamesDb))
    if len(extraCols) > 0:
        print('There are columns present in source data but not database table.')
        print(extraCols)
        print('You will need to add these columns to the database table or drop the table and start over.')

# %% if no issues, load CLEAResult data

# run function to insert and update records 
insertCnt, updateCnt = insertAndUpdate(tableName, schema, uniqueColumns, crDb, crDataDf)
print("Wrote " + insertCnt + " inserts to " + schema + '.' + tableName)
print("Wrote " + updateCnt + " updates to " + schema + '.' + tableName)

#%% load CenterPoint data to tracking

# creates dataframe using load_data function

cpDataDf = load_data("CenterPoint")

cpDataDf = cpDataDf.loc[:,~cpDataDf.columns.duplicated()].copy()
cpDataDf.columns = cpDataDf.columns.str.replace('\n', '')


# remove empty extra rows 
cpDataDf = cpDataDf.loc[~cpDataDf['visiondsm_project_number'].isnull(),]
cpDataDf['utility'] = 'CenterPoint'

# fix invalid column names 
cpDataDf.rename(columns={'premise_account_#': 'premise_account'}, inplace=True)

# create harmonized columns using loaded crosswalk table
tableName = 'centerPointTracking'
cpDataDf = harmonizeColumns(tableName, cpDataDf, xwalk)

# Ask Jess about unique columns

uniqueColumns = ['visiondsm_program', 'visiondsm_project_number', 'measure_code', 'installation_id']
recordCount = len(cpDataDf)
recordCountU = len(cpDataDf[uniqueColumns].drop_duplicates())
if recordCount > recordCountU:
    print("Double-check your list of unique columns, it seems to be incomplete.")


# if db table exists, check to be sure all source columns are present 
cpDb = pullTrackingData(tableName, schema)
colNamesDb = cpDb.columns.tolist()
if len(colNamesDb) > 1:
    colNamesSource = cpDataDf.columns.tolist()
    extraCols = list(set(colNamesSource) - set(colNamesDb))
    if len(extraCols) > 0:
        print('There are columns present in source data but not database table.')
        print(extraCols)
        print('You will need to add these columns to the database table or drop the table and start over.')

# %% if no issues, load CenterPoint data

# run function to insert and update records 
insertCnt, updateCnt = insertAndUpdate(tableName, schema, uniqueColumns, cpDb, cpDataDf)
print("Wrote " + insertCnt + " inserts to " + schema + '.' + tableName)
print("Wrote " + updateCnt + " updates to " + schema + '.' + tableName)

# %% load Oncor data to tracking

# creates dataframe using load_data function
oncorDataDf = load_data("Oncor")

# remove empty extra rows  
oncorDataDf = oncorDataDf.loc[~oncorDataDf['program_name'].isnull(),] 
oncorDataDf['utility'] = 'Oncor'

# check for any with missing Sector
missingSector = oncorDataDf.loc[oncorDataDf['sector'].isnull(),
                                ['sector', 'program_name']].drop_duplicates()
if len(missingSector) > 0:
    print('These programs need a Sector assigned')
    print(missingSector)

# create harmonized columns using loaded crosswalk table
tableName = 'oncorTracking'
oncorDataDf = harmonizeColumns(tableName, oncorDataDf, xwalk)

uniqueColumns = ['program_name', 'workgroup_code', 'measure_name', 'measure_life', 'esiid']
recordCount = len(oncorDataDf)
recordCountU = len(oncorDataDf[uniqueColumns].drop_duplicates())
if recordCount > recordCountU:
    print("Double-check your list of unique columns, it seems to be incomplete.")

# if db table exists, check to be sure all source columns are present 
oncorDb = pullTrackingData(tableName, schema)
colNamesDb = oncorDb.columns.tolist()
if len(colNamesDb) > 1:
    colNamesSource = oncorDataDf.columns.tolist()
    extraCols = list(set(colNamesSource) - set(colNamesDb))
    if len(extraCols) > 0:
        print('There are columns present in source data but not database table.')
        print(extraCols)
        print('You will need to add these columns to the database table or drop the table and start over.')

# %% if all okay, load Oncor data

# run function to insert and update records 
insertCnt, updateCnt = insertAndUpdate(tableName, schema, uniqueColumns, oncorDb, oncorDataDf)
print("Wrote " + insertCnt + " inserts to " + schema + '.' + tableName)
print("Wrote " + updateCnt + " updates to " + schema + '.' + tableName)

# %%load Oncor RPP data to tracking

# creates dataframe using load_data function
oncorDataRPPDf = load_data('Oncor/2021 Q1-Q4 RES & Comm Solar & RPP Tracking Data')

# this file contains both Residential and Commercial data, need to split out 
oncorDataRPPDf['sector'] = 'Residential'
oncorDataRPPDf['kw'] = oncorDataRPPDf['residential_kw'].mask(pd.isnull, oncorDataRPPDf['kw_savings'])
oncorDataRPPDf['kwh'] = oncorDataRPPDf['residential_kwh'].mask(pd.isnull, oncorDataRPPDf['kwh_savings'])
oncorDataRPPDf1 = oncorDataRPPDf.copy()
oncorDataRPPDf1['sector'] = 'Commercial'
oncorDataRPPDf1['kw'] = oncorDataRPPDf1['commercial_kw'].mask(pd.isnull, 0)
oncorDataRPPDf1['kwh'] = oncorDataRPPDf1['commercial_kwh'].mask(pd.isnull, 0)
oncorDataRPPDf = pd.concat([oncorDataRPPDf, oncorDataRPPDf1])
oncorDataRPPDf.drop(columns=['residential_kw', 'residential_kwh', 'commercial_kw', 'commercial_kwh'],
                    inplace=True)

# check for any with missing Sector
missingSector = oncorDataRPPDf.loc[oncorDataRPPDf['sector'].isnull(),
                                   ['sector', 'program_name']].drop_duplicates()
if len(missingSector) > 0:
    print('These programs need a Sector assigned')
    print(missingSector)

# create harmonized columns using loaded crosswalk table
tableName = 'oncorRPPTracking'
oncorDataRPPDf = harmonizeColumns(tableName, oncorDataRPPDf, xwalk)

uniqueColumns = ['sector', 'program_name', 'workgroup_code', 'measure_name', 'measure_life', 'esiid']
recordCount = len(oncorDataRPPDf)
recordCountU = len(oncorDataRPPDf[uniqueColumns].drop_duplicates())
if recordCount > recordCountU:
    print("Double-check your list of unique columns, it seems to be incomplete.")

# if db table exists, check to be sure all source columns are present 
oncorRPPDb = pullTrackingData(tableName, schema)
colNamesDb = oncorRPPDb.columns.tolist()
if len(colNamesDb) > 1:
    colNamesSource = oncorDataRPPDf.columns.tolist()
    extraCols = list(set(colNamesSource) - set(colNamesDb))
    if len(extraCols) > 0:
        print('There are columns present in source data but not database table.')
        print(extraCols)
        print('You will need to add these columns to the database table or drop the table and start over.')

# %% if all okay, load Oncor RPP data

# run function to insert and update records 
insertCnt, updateCnt = insertAndUpdate(tableName, schema, uniqueColumns, oncorRPPDb, oncorDataRPPDf)
print("Wrote " + insertCnt + " inserts to " + schema + '.' + tableName)
print("Wrote " + updateCnt + " updates to " + schema + '.' + tableName)

# %% load EPE data to tracking

# EPE files must be processed separately because they are different formats 

epeAppDataDf = load_data('EPE')
epeAppDataDf['program_name'] = 'Appliance Recycling'
epeAppDataDf['utility'] = 'El Paso Electric'
epeAppDataDf['sector'] = 'Residential'

'''
epeMarketplaceDataDf = load_data('EPE')
epeMarketplaceDataDf['program_name'] = 'Marketplace Savings'
epeMarketplaceDataDf['utility'] = 'El Paso Electric'
# remove extra rows 
epeMarketplaceDataDf = epeMarketplaceDataDf.loc[~epeMarketplaceDataDf['utility_name'].isnull(),]
# this file contains both Residential and Commercial data, need to split out 
epeMarketplaceDataDf['sector'] = 'Residential'
epeMarketplaceDataDf['kw'] = epeMarketplaceDataDf['res_kw']
epeMarketplaceDataDf['kwh'] = epeMarketplaceDataDf['res_kwh']
epeMarketplaceDataDf['lifetime_kWh'] = epeMarketplaceDataDf['res_lifetime_kwh']
epeMarketplaceDataDf['eul'] = epeMarketplaceDataDf['res_eul']
epeMarketplaceDataDf1 = epeMarketplaceDataDf.copy()
epeMarketplaceDataDf1['sector'] = 'Commercial'
epeMarketplaceDataDf1['kw'] = epeMarketplaceDataDf1['com_kw']
epeMarketplaceDataDf1['kwh'] = epeMarketplaceDataDf1['com_kwh']
epeMarketplaceDataDf1['lifetime_kWh'] = epeMarketplaceDataDf1['com_lifetime_kwh']
epeMarketplaceDataDf1['eul'] = epeMarketplaceDataDf1['com_eul']
epeMarketplaceDataDf = pd.concat([epeMarketplaceDataDf, epeMarketplaceDataDf1])
epeMarketplaceDataDf.drop(columns=['res_kw', 'res_kwh', 'res_lifetime_kwh', 'res_eul',
                                   'com_kw', 'com_kwh', 'com_lifetime_kwh', 'com_eul'],
                          inplace=True)
'''
# check for any with missing Sector
# missingSector = epeMarketplaceDataDf.loc[epeMarketplaceDataDf['sector'].isnull(),
                                        # ['sector', 'program_name']].drop_duplicates()
missingSector2 = epeAppDataDf.loc[epeAppDataDf['sector'].isnull(),
                                  ['sector', 'program_name']].drop_duplicates()
'''if len(missingSector) > 0:
    print('These programs need a Sector assigned')
    print(missingSector)'''
if len(missingSector2) > 0:
    print('These programs need a Sector assigned')
    print(missingSector2)

# create harmonized columns using loaded crosswalk table
'''tableName = 'epeMarketplaceTracking'
epeMarketplaceDataDf = harmonizeColumns(tableName, epeMarketplaceDataDf, xwalk)'''
tableName = 'epeApplianceTracking'
epeAppDataDf = harmonizeColumns(tableName, epeAppDataDf, xwalk)

uniqueColumns = ['ato_number']
recordCount = len(epeAppDataDf)
recordCountU = len(epeAppDataDf[uniqueColumns].drop_duplicates())
if recordCount > recordCountU:
    print("Double-check your list of unique columns, it seems to be incomplete.")

# if db table exists, check to be sure all source columns are present 
epeAppDataDb = pullTrackingData(tableName, schema)
colNamesDb = epeAppDataDb.columns.tolist()
if len(colNamesDb) > 1:
    colNamesSource = epeAppDataDf.columns.tolist()
    extraCols = list(set(colNamesSource) - set(colNamesDb))
    if len(extraCols) > 0:
        print('There are columns present in source data but not database table.')
        print(extraCols)
        print('You will need to add these columns to the database table or drop the table and start over.')

# %% if all okay, load EPE data

# run function to insert and update records 
insertCnt, updateCnt = insertAndUpdate(tableName, schema, uniqueColumns, epeAppDataDb, epeAppDataDf)
print("Wrote " + insertCnt + " inserts to " + schema + '.' + tableName)
print("Wrote " + updateCnt + " updates to " + schema + '.' + tableName)

# Marketplace data doesn't have fields to identify unique records
# also, this data gets rolled up in the main tables, so not important 
# in this case, truncate table if exists and load data 
'''tableName = 'epeMarketplaceTracking'
epeMarketplaceDataDb = pullTrackingData(tableName, schema)
insertCnt = truncateAndInsert(tableName, schema, uniqueColumns, epeMarketplaceDataDb, epeMarketplaceDataDf)
print("Wrote " + insertCnt + " inserts to " + schema + '.' + tableName)'''

# %% load Willdan data to tracking

# list of Willdan sub_directories and files 
willdanDataDf = load_data('Willdan Xcel RCx')
willdanDataDf = willdanDataDf.loc[~willdanDataDf['utility_name'].isnull(),]
willdanDataDf.loc[willdanDataDf['utility_name'].str.contains('Xcel'), 'utility_name'] = 'Xcel'
willdanDataDf['sector'] = 'Commercial'

# check for any with missing Sector
missingSector = willdanDataDf.loc[willdanDataDf['sector'].isnull(),
                                  ['sector', 'program']].drop_duplicates()
if len(missingSector) > 0:
    print('These programs need a Sector assigned')
    print(missingSector)

# create harmonized columns using loaded crosswalk table
tableName = 'willdanTracking'
willdanDataDf = harmonizeColumns(tableName, willdanDataDf, xwalk)

# %% if all okay, load Willdan data

# Willdan retrocommissioning data doesn't have fields to identify unique records
# also, this data gets rolled up in the main tables, so not important 
# in this case, truncate table if exists and load data 
tableName = 'willdanTracking'
willdanDataDb = pullTrackingData(tableName, schema)
insertCnt = truncateAndInsert(tableName, schema, uniqueColumns, willdanDataDb, willdanDataDf)
print("Wrote " + insertCnt + " inserts to " + schema + '.' + tableName)

# %% load Xcel data to tracking

# list of Xcel sub_directories and files 

xcelDataDf = load_data('Xcel')
# remove extra rows 
xcelDataDf = xcelDataDf.loc[~xcelDataDf['importid'].isnull(),]
xcelDataDf['utility'] = 'Xcel'
xcelDataDf['program_name'] = 'Home Lighting'
xcelDataDf['sector'] = xcelDataDf['res_and_com_']
xcelDataDf['measure_name'] = 'Specialty LED'
if 'model_type' in xcelDataDf:
    xcelDataDf.loc[xcelDataDf['model_type'] == 'A-line', 'measure_name'] = 'Omnidirectional LED'

# check for any with missing Sector
missingSector = xcelDataDf.loc[xcelDataDf['sector'].isnull(),
                               ['sector', 'program_name']].drop_duplicates()
if len(missingSector) > 0:
    print('These programs need a Sector assigned')
    print(missingSector)

# create harmonized columns using loaded crosswalk table
tableName = 'xcelHomeLightingTracking'
# xcelDataDf = harmonizeColumns(tableName, xcelDataDf, xwalk)

# %% if all okay, load Xcel data

# Home Lighting data doesn't have fields to identify unique records
# also, this data gets rolled up in the main tables, so not important 
# in this case, truncate table if exists and load data 
tableName = 'xcelHomeLightingTracking'
xcelDataDb = pullTrackingData(tableName, schema)
insertCnt = truncateAndInsert(tableName, schema, uniqueColumns, xcelDataDb, xcelDataDf)
print("Wrote " + insertCnt + " inserts to " + schema + '.' + tableName)

#%% load Xcel smart thermostat data to tracking

data_provider = 'Xcel'
file_name = 'TX T-Stat Mid-Year Data.csv'
xcelTstat = pd.read_csv(dataDir+'Xcel/'+file_name, low_memory=False, dtype=str)
xcelTstat["sector"] = 'Residential'
xcelTstat["data_provider"] = data_provider
xcelTstat["file_name"] = file_name

# remove extra rows 
xcelTstat = xcelTstat.loc[~xcelTstat['Invoice Number'].isnull(),]
xcelTstat['utility'] = 'Xcel'
xcelTstat['program_name'] = 'Smart Thermostat'
xcelTstat['measure_name'] = 'Smart Thermostat'

# check for any with missing Sector
missingSector = xcelTstat.loc[xcelTstat['sector'].isnull(),
                               ['sector', 'program_name']].drop_duplicates()
if len(missingSector) > 0:
    print('These programs need a Sector assigned')
    print(missingSector)

# create harmonized columns using loaded crosswalk table
tableName = 'xcelHomeLightingTracking'
# xcelDataDf = harmonizeColumns(tableName, xcelDataDf, xwalk)

# %% Pull PYID info from db for program year

conn = pyodbc.connect(
    'Driver={SQL Server};Server=etss704sqldevqa\ETSS704SQL12_QA;Database=TX_EMV_PUCT;Trusted_Connection=yes;')

# pull matches and make sure they exist 
sql_statement = 'select * from ' + schema + '.ProgramYearList'

existingPrograms = pd.read_sql(sql_statement, conn)
existingPrograms = existingPrograms[['ProgramYearID', 'UtilityID', 'UtilityName', 'SectorID', 'SectorName',
                                     'ReportSectorID', 'ReportSectorDescription', 'UtilityProgramName',
                                     'ProgramName', 'ProgramGroupName', 'ProgramTypeName']]

# %% if exists, pull current lookup data

sqlQuery1 = "If exists (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='"
sqlQuery2 = "' AND TABLE_NAME='pyidLookup') select * from "
sqlQuery3 = ".pyidLookup else select '' as ProgramYearID, '' as Utility, '' as UtilityProgramName, '' as Sector;"
sqlQuery = sqlQuery1 + schema + sqlQuery2 + schema + sqlQuery3

existingLookup = pd.read_sql(sqlQuery, conn)

# %% summarize data for lookup matching


# get provider, file_name, utility, and programs from each source df 
providers = ['frontierAgg', 'crAgg', 'cpAgg', 'oncorAgg', 'oncorRPPAgg', 'epeAppAgg', 'epeMarketplaceAgg', 'willdanAgg', 'xcelAgg']
sourcedf = [frontierDataDf, crDataDf, cpDataDf, oncorDataDf, oncorDataRPPDf, epeAppDataDf, epeMarketplaceDataDf, willdanDataDf, xcelDataDf]
for p1,d1 in zip(providers,sourcedf):
    globals()[p1] = d1.groupby(['data_provider', 'sub_dir', 'utility', 'file_name', 'program_name', 'sector'],
                                     as_index=False).agg(recordCount=('project_id', 'count'))
    globals()[p1].columns = ['data_provider', 'sub_dir', 'Utility', 'file_name', 'ProgramName', 'Sector', 'recordCount']

trackingSummary = pd.concat([frontierAgg, crAgg, cpAgg, oncorAgg, oncorRPPAgg, epeAppAgg,
                             epeMarketplaceAgg, willdanAgg, xcelAgg])

# %% summarize data for tracking matching

# summarize tracking for comparison to what was loaded
loaded = [frontierDataDf, oncorDataDf, oncorDataRPPDf, epeAppDataDf, epeMarketplaceDataDf,xcelDataDf]
for df in loaded: 
    trackingSummaryDb = pd.concat([df.loc[:, ['utility', 'program_name', 'sector',
                                                      'data_provider', 'exante_savings_kw', 'exante_savings_kwh',
                                                      'incentive']],
                               crDataDf.loc[:, ['utility_client', 'program_name', 'market_sector',
                                                'data_provider', 'exante_savings_kw', 'exante_savings_kwh',
                                                'incentive']].rename(columns={'utility_client': 'utility',
                                                                              'market_sector': 'sector'}),
                               cpDataDf.loc[:, ['utility', 'visiondsm_program', 'sector', 'data_provider',
                                                'exante_savings_kw', 'exante_savings_kwh', 'incentive']].rename(
                                   columns={'visiondsm_program': 'program_name'}),
                               willdanDataDf.loc[:, ['utility_name', 'program', 'sector', 'data_provider',
                                                     'exante_savings_kw', 'exante_savings_kwh', 'incentive']].rename(
                                   columns={'program': 'program_name',
                                            'utility_name': 'utility'})]])

trackingDbSummary = trackingSummaryDb.groupby(['utility', 'program_name', 'sector'],
                                              as_index=False).agg(recordCount=('data_provider', 'count'),
                                                                  sumkW=('exante_savings_kw', sum),
                                                                  sumkWh=('exante_savings_kwh', sum),
                                                                  sumIncentive=('incentive', sum))

# %% pull in data loaded to tracking tables

sqlQuery = """
select utility, program_name, sector, 
case when recordCount is null then 0 else recordCount end as recordCountdb, 
case when sumKW is null then 0 else sumKW end as sumKWdb, 
case when sumKWH is null then 0 else sumKWH end as sumKWHdb, 
case when sumIncentive is null then 0 else sumIncentive end as sumIncentivedb
from (
select utility, program_name, sector, count(*) as recordCount, sum(exante_savings_kw) as sumKW, 
sum(exante_savings_kwh) as sumKWH, sum(incentive) as sumIncentive 
from TX2021.frontierTracking group by utility, program_name, sector
union 
select utility_client as utility, program_name, market_sector as sector, count(*) as recordCount, sum(exante_savings_kw) as sumKW, 
sum(exante_savings_kwh) as sumKWH, sum(incentive) as sumIncentive 
from TX2021.CLEAResultTracking group by utility_client, program_name, market_sector
union 
select utility, visiondsm_program as program_name, sector, count(*) as recordCount, sum(exante_savings_kw) as sumKW, 
sum(exante_savings_kwh) as sumKWH, sum(incentive) as sumIncentive 
from TX2021.centerPointTracking group by utility, visiondsm_program, sector
union 
select utility, program_name, sector, count(*) as recordCount, sum(exante_savings_kw) as sumKW, 
sum(exante_savings_kwh) as sumKWH, sum(incentive) as sumIncentive 
from TX2021.oncorTracking group by utility, program_name, sector
union 
select utility, program_name, sector, count(*) as recordCount, sum(exante_savings_kw) as sumKW, 
sum(exante_savings_kwh) as sumKWH, sum(incentive) as sumIncentive 
from TX2021.oncorRPPTracking group by utility, program_name, sector
union 
select utility, program_name, sector, count(*) as recordCount, sum(exante_savings_kw) as sumKW, 
sum(exante_savings_kwh) as sumKWH, sum(incentive) as sumIncentive 
from TX2021.epeApplianceTracking group by utility, program_name, sector
union 
select utility as utility, program_name, sector, count(*) as recordCount, sum(exante_savings_kw) as sumKW, 
sum(exante_savings_kwh) as sumKWH, sum(incentive) as sumIncentive 
from TX2021.epeMarketplaceTracking group by utility, program_name, sector
union 
select utility_name as utility, program as program_name, sector, count(*) as recordCount, sum(exante_savings_kw) as sumKW, 
sum(exante_savings_kwh) as sumKWH, sum(incentive) as sumIncentive 
from TX2021.willdanTracking group by utility_name, program, sector
union 
select utility, program_name, sector, count(*) as recordCount, sum(exante_savings_kw) as sumKW, 
sum(exante_savings_kwh) as sumKWH, sum(incentive) as sumIncentive 
from TX2021.xcelHomeLightingTracking group by utility, program_name, sector
) as a
"""

currentTracking = pd.read_sql(sqlQuery, conn)

# %% compare source to loaded

compareData = trackingDbSummary.merge(currentTracking, on=['utility', 'program_name', 'sector'],
                                      how='outer', indicator=True)
# replace nans with 0 for comparison 
compareData = compareData.apply(lambda x: x.fillna(0) if x.dtype.kind in ['float64', 'int64'] else x)
# retain any with differences 
compareDataIssues = compareData.loc[(round(compareData['sumkW'] - compareData['sumKWdb'], 2) != 0) |
                                    (round(compareData['recordCount'] - compareData['recordCountdb'], 2) != 0) |
                                    (round(compareData['sumkWh'] - compareData['sumKWHdb'], 2) != 0) |
                                    (round(compareData['sumIncentive'] - compareData['sumIncentivedb'], 2) != 0),]

if len(compareDataIssues) > 0:
    print('The following programs have differences in source data and loaded data')
    print('This may or may not be an issue, it could be expected that loaded data has more records')
    print(compareDataIssues)
else:
    print('Source data and loaded tracking data counts and sums match for all programs')

# %% check on whether PYIDs have been assigned for programs received

pyIDs = currentTracking.merge(existingLookup, right_on=['Utility', 'Sector', 'UtilityProgramName'],
                              left_on=['utility', 'sector', 'program_name'], how='outer')

pyUnassigned = pyIDs.loc[pyIDs['UtilityProgramName'].isnull(),]
if len(pyUnassigned) > 0:
    print('These source programs need a Program Year ID assigned')
    print(pyUnassigned[['utility', 'program_name', 'sector']])

# %% output checking data to file

# add data_provider to tracking table 
trackTable['data_provider'] = trackTable['Provider of Data'].str.replace('Implementer-', '').str.replace(
    'Implementer -', '')
trackTable['data_provider'] = trackTable['data_provider'].str.strip()
trackTable.loc[trackTable['data_provider'] == 'Utility', 'data_provider'] = trackTable['Utility']

tableOut = 'trackingDataCheck' + datetime.datetime.now().strftime('%Y%m%d') + '.csv'
# Create a Pandas Excel writer using csvWriter as the engine.
writer = pd.ExcelWriter(dataDir + tableOut, engine='csvwriter')

# Write each dataframe to a different worksheet.
trackingSummary.sort_values(by=['Utility', 'Sector', 'ProgramName']).to_excel(writer, sheet_name='receivedSummary',
                                                                              index=False)
trackTable[['Utility', 'Program Type', 'Program Name', 'Technical Specs', 'Description',
            'data_provider', 'Internal Notes', 'Priority',
            'Loaded/File Name']].sort_values(by=['Utility', 'Program Type',
                                                 'Program Name']).to_excel(writer, sheet_name='trackingFile',
                                                                           index=False)
pyIDs.to_excel(writer, sheet_name='pyidLookupAndNewPrograms', index=False)
existingPrograms.to_excel(writer, sheet_name='ProgramYearList', index=False)
compareData.to_excel(writer, sheet_name='sourceLoadComparisonSummary', index=False)

# Close the Pandas Excel writer and output the Excel file.
writer.save()

print('Load information saved to table ' + tableOut)
print(
    'Use this file to assign any needed Program Year IDs, check that all data expected was received, and investigate any differences in source and loaded data.')
