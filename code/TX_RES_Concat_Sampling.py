#%% -*- coding: utf-8 -*-
"""
Created on Wed July 05 15:25:04 2022

@author: SIMRAN.PADAM
"""
from re import T, X
from sys import displayhook
import pandas as pd
import pyodbc
import numpy as np
import os
from datetime import datetime
from sqlalchemy import create_engine
from pathlib import Path
import glob
from IPython.display import display
from openpyxl import load_workbook
from collections import Counter
import itertools
from math import nan, isnan

data_directory = "F:\Texas PUC 2021-2024\Sample\PY2022\Residential\On_Site"
conn = pyodbc.connect('''Driver={SQL Server};
                      Server=etss704sqldevqa\ETSS704SQL12_QA;
                      Database=TX_EMV_PUCT;
                      Trusted_Connection=yes;''')
output_folder = 'F:\Texas PUC 2021-2024\Sample\PY2022\Residential\\'
cursor = conn.cursor()
con_str = ('mssql+pyodbc://etss704sqldevqa\ETSS704SQL12_QA/' + 'TX_EMV_PUCT' + '?trusted_connection=yes&driver=SQL Server Native Client 11.0')
engine = create_engine(con_str, fast_executemany=True)

#%%
def load_data(data_directory,sheet):

    # Empty dataframe for above files
    df = []

    # Using glob grabs all excel files in the directory and concatenates it into one df
    files = glob.glob(data_directory + "\*.xlsx")
    for file in files:
        excel = pd.read_excel(file, sheet_name=sheet)
        df.append(excel)
        df2 = pd.concat(df)
    return df2 

# Running the above function and assigning the concatenated df to "sbdf"
sampledf = load_data(data_directory,'Samples_OS')
attributedf = load_data(data_directory,'Sample_Attributes_OS')

#%% Add customer info to samples
query = f'''
    SELECT UtilityProgramName, participant_id, participant_code, esiid, customer_name, mail_address1, mail_address2, mail_city, mail_state, mail_zip_code, email_address, primary_phone,  program_year_id, UtilityName, ProgramYear, UtilityProgramName
    , ProgramName, ProgramDescription, SectorName, ReportSectorDescription, ProgramTypeName, ProgramGroupName, measure_description, measure_reporting_description
    , source_measure_code, realization_rate_kw, realization_rate_kwh, MeasureCategoryName, project_id, source_data_row_id, participant_impact_date, count, quantity
    , incentive, exante_savings_kw, exante_savings_kwh,trade_ally_zip, UtilityID, ProgramID, SectorID, ProgramTypeID
    , ProgramGroupID, measure_id, quantity_unit, measure_category_id, PrecisionRankName, ReportSectorSort, eul, EvaluationPriority, ReportSectorID
    FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
    '''
info = pd.read_sql(query, conn)

sampledf_list = attributedf['esiid'].tolist()
samplecnp_list = attributedf['participant_id'].tolist()
samples = info[info['esiid'].isin(sampledf_list)]
samplescnp = info[info['participant_id'].isin(samplecnp_list)]
total_samples = pd.concat([samples, samplescnp])
# %% Save to excel 
writer = pd.ExcelWriter(output_folder + "For Distribution\PY22 Q1-Q2 Residential OS.xlsx")
    
    # writing the characterized and initial population df to each files sheet in excel
sampledf.to_excel(writer, sheet_name='Samples', index=False)
samplescnp.to_excel(writer, sheet_name='Samples_Attributes', index=False)
writer.save()
# %%
