# -*- coding: utf-8 -*-
"""
Created on Thu Apr  7 11:12:48 2022

@author: JESSICA.THOMPSON
This script creates and readies the db schema for the new Program Year, based on the previous year
update dbSchema and dbSchemaOld before running this script
"""

#%% set data directory and variables

import pyodbc 

dbSchema = 'TX2022'
dbSchemaOld = 'TX2021'

#%% create new db schemas (staging and prod)

# staging
connStg = pyodbc.connect('Driver={SQL Server};Server=etss704sqldevqa,50179;Database=TX_EMV_PUCT;Trusted_Connection=yes;')
cursorStg = connStg.cursor()
cursorStg.execute('create schema '+dbSchema+';')
connStg.commit()

# prod
connProd = pyodbc.connect('Driver={SQL Server};Server=etss704sql1,45452;Database=TX_EMV_PUCT;Trusted_Connection=yes;')
cursorProd = connProd.cursor()
cursorProd.execute('create schema '+dbSchema+';')
connProd.commit()

#%% create tables in new schemas 

# staging 
cursorStg.execute('select * into '+dbSchema+'.Measure from '+dbSchemaOld+'.Measure where 1=2;')
cursorStg.execute('select * into '+dbSchema+'.Participant from '+dbSchemaOld+'.Participant where 1=2;')
cursorStg.execute('select * into '+dbSchema+'.ParticipantImpact from '+dbSchemaOld+'.ParticipantImpact where 1=2;')
cursorStg.execute('select * into '+dbSchema+'.ProgramYear from '+dbSchemaOld+'.ProgramYear where 1=2;')
cursorStg.execute('select * into '+dbSchema+'.TradeAlly from '+dbSchemaOld+'.TradeAlly where 1=2;')
cursorStg.execute('select * into '+dbSchema+'.UtilityInputs from '+dbSchemaOld+'.UtilityInputs where 1=2;')
connStg.commit()
# add PKs
cursorStg.execute('alter table '+dbSchema+'.Measure add constraint Measure_UN UNIQUE (program_year_id,measure_description,source_measure_code,eul,source_measure_id);')
cursorStg.execute('alter table '+dbSchema+'.Measure add constraint PK_Measure_ident PRIMARY KEY (measure_id);')
cursorStg.execute('alter table '+dbSchema+'.Participant add CONSTRAINT PK_Participant_ident PRIMARY KEY (participant_id);')
cursorStg.execute('alter table '+dbSchema+'.ParticipantImpact add CONSTRAINT PK_ParticipantImpact_ident PRIMARY KEY (participant_impact_id);')
cursorStg.execute('alter table '+dbSchema+'.ProgramYear add CONSTRAINT PK_ProgramYear PRIMARY KEY (ProgramYearID);')
cursorStg.execute('alter table '+dbSchema+'.TradeAlly add CONSTRAINT PK_TradeAlly_ident PRIMARY KEY (trade_ally_id);')
cursorStg.execute('alter table '+dbSchema+'.UtilityInputs add CONSTRAINT PK_UtilityInputs PRIMARY KEY (UtilityID,ProgramYear);')
connStg.commit()

# prod 
cursorProd.execute('select * into '+dbSchema+'.Measure from '+dbSchemaOld+'.Measure where 1=2;')
cursorProd.execute('select * into '+dbSchema+'.Participant from '+dbSchemaOld+'.Participant where 1=2;')
cursorProd.execute('select * into '+dbSchema+'.ParticipantImpact from '+dbSchemaOld+'.ParticipantImpact where 1=2;')
cursorProd.execute('select * into '+dbSchema+'.ProgramYear from '+dbSchemaOld+'.ProgramYear where 1=2;')
cursorProd.execute('select * into '+dbSchema+'.TradeAlly from '+dbSchemaOld+'.TradeAlly where 1=2;')
cursorProd.execute('select * into '+dbSchema+'.UtilityInputs from '+dbSchemaOld+'.UtilityInputs where 1=2;')
connProd.commit()
# add PKs
cursorProd.execute('alter table '+dbSchema+'.Measure add constraint Measure_UN UNIQUE (program_year_id,measure_description,source_measure_code,eul,source_measure_id);')
cursorProd.execute('alter table '+dbSchema+'.Measure add constraint PK_Measure_ident PRIMARY KEY (measure_id);')
cursorProd.execute('alter table '+dbSchema+'.Participant add CONSTRAINT PK_Participant_ident PRIMARY KEY (participant_id);')
cursorProd.execute('alter table '+dbSchema+'.ParticipantImpact add CONSTRAINT PK_ParticipantImpact_ident PRIMARY KEY (participant_impact_id);')
cursorProd.execute('alter table '+dbSchema+'.ProgramYear add CONSTRAINT PK_ProgramYear PRIMARY KEY (ProgramYearID);')
cursorProd.execute('alter table '+dbSchema+'.TradeAlly add CONSTRAINT PK_TradeAlly_ident PRIMARY KEY (trade_ally_id);')
cursorProd.execute('alter table '+dbSchema+'.UtilityInputs add CONSTRAINT PK_UtilityInputs PRIMARY KEY (UtilityID,ProgramYear);')
connProd.commit()


#%% create constraints in new schemas 

# staging 
cursorStg.execute('alter table '+dbSchema+'.UtilityInputs add constraint FK_UtilityInputs_Utility'+dbSchema+' foreign key (UtilityID) REFERENCES lookup.Utility(UtilityID);')
cursorStg.execute('alter table '+dbSchema+'.ProgramYear add constraint FK_ProgramYear_PrecisionRank'+dbSchema+' FOREIGN KEY (PrecisionRankID) REFERENCES lookup.PrecisionRank(PrecisionRankID);')
cursorStg.execute('ALTER TABLE '+dbSchema+'.ProgramYear ADD CONSTRAINT FK_ProgramYear_Program'+dbSchema+' FOREIGN KEY (ProgramID) REFERENCES lookup.Program(ProgramID);')
cursorStg.execute('ALTER TABLE '+dbSchema+'.ProgramYear ADD CONSTRAINT FK_ProgramYear_Sector'+dbSchema+' FOREIGN KEY (SectorID) REFERENCES lookup.Sector(SectorID);')
cursorStg.execute('ALTER TABLE '+dbSchema+'.ProgramYear ADD CONSTRAINT FK_ProgramYear_ReportSector'+dbSchema+' FOREIGN KEY (ReportSectorID) REFERENCES lookup.ReportSector(ReportSectorID);')
cursorStg.execute('ALTER TABLE '+dbSchema+'.ProgramYear ADD CONSTRAINT FK_ProgramYear_Utility'+dbSchema+' FOREIGN KEY (UtilityID) REFERENCES lookup.Utility(UtilityID);')
cursorStg.execute('ALTER TABLE '+dbSchema+'.ParticipantImpact ADD CONSTRAINT FK_ParticipantImpact_Measure'+dbSchema+' FOREIGN KEY (measure_id) REFERENCES '+dbSchema+'.Measure(measure_id);')
cursorStg.execute('ALTER TABLE '+dbSchema+'.ParticipantImpact ADD CONSTRAINT FK_ParticipantImpact_Participant'+dbSchema+' FOREIGN KEY (participant_id) REFERENCES '+dbSchema+'.Participant(participant_id);')
cursorStg.execute('ALTER TABLE '+dbSchema+'.Participant ADD CONSTRAINT FK_Participant_ProgramYear'+dbSchema+' FOREIGN KEY (program_year_id) REFERENCES '+dbSchema+'.ProgramYear(ProgramYearID);')
cursorStg.execute('ALTER TABLE '+dbSchema+'.Measure ADD CONSTRAINT FK_Measure_MeasureCategory'+dbSchema+' FOREIGN KEY (measure_category_id) REFERENCES lookup.MeasureCategory(MeasureCategoryID);')
cursorStg.execute('ALTER TABLE '+dbSchema+'.Measure ADD CONSTRAINT FK_Measure_ProgramYear'+dbSchema+' FOREIGN KEY (program_year_id) REFERENCES '+dbSchema+'.ProgramYear(ProgramYearID);')
connStg.commit()

# prod 
cursorProd.execute('alter table '+dbSchema+'.UtilityInputs add constraint FK_UtilityInputs_Utility'+dbSchema+' foreign key (UtilityID) REFERENCES lookup.Utility(UtilityID);')
cursorProd.execute('alter table '+dbSchema+'.ProgramYear add constraint FK_ProgramYear_PrecisionRank'+dbSchema+' FOREIGN KEY (PrecisionRankID) REFERENCES lookup.PrecisionRank(PrecisionRankID);')
cursorProd.execute('ALTER TABLE '+dbSchema+'.ProgramYear ADD CONSTRAINT FK_ProgramYear_Program'+dbSchema+' FOREIGN KEY (ProgramID) REFERENCES lookup.Program(ProgramID);')
cursorProd.execute('ALTER TABLE '+dbSchema+'.ProgramYear ADD CONSTRAINT FK_ProgramYear_Sector'+dbSchema+' FOREIGN KEY (SectorID) REFERENCES lookup.Sector(SectorID);')
cursorProd.execute('ALTER TABLE '+dbSchema+'.ProgramYear ADD CONSTRAINT FK_ProgramYear_ReportSector'+dbSchema+' FOREIGN KEY (ReportSectorID) REFERENCES lookup.ReportSector(ReportSectorID);')
cursorProd.execute('ALTER TABLE '+dbSchema+'.ProgramYear ADD CONSTRAINT FK_ProgramYear_Utility'+dbSchema+' FOREIGN KEY (UtilityID) REFERENCES lookup.Utility(UtilityID);')
cursorProd.execute('ALTER TABLE '+dbSchema+'.ParticipantImpact ADD CONSTRAINT FK_ParticipantImpact_Measure'+dbSchema+' FOREIGN KEY (measure_id) REFERENCES '+dbSchema+'.Measure(measure_id);')
cursorProd.execute('ALTER TABLE '+dbSchema+'.ParticipantImpact ADD CONSTRAINT FK_ParticipantImpact_Participant'+dbSchema+' FOREIGN KEY (participant_id) REFERENCES '+dbSchema+'.Participant(participant_id);')
cursorProd.execute('ALTER TABLE '+dbSchema+'.Participant ADD CONSTRAINT FK_Participant_ProgramYear'+dbSchema+' FOREIGN KEY (program_year_id) REFERENCES '+dbSchema+'.ProgramYear(ProgramYearID);')
cursorProd.execute('ALTER TABLE '+dbSchema+'.Measure ADD CONSTRAINT FK_Measure_MeasureCategory'+dbSchema+' FOREIGN KEY (measure_category_id) REFERENCES lookup.MeasureCategory(MeasureCategoryID);')
cursorProd.execute('ALTER TABLE '+dbSchema+'.Measure ADD CONSTRAINT FK_Measure_ProgramYear'+dbSchema+' FOREIGN KEY (program_year_id) REFERENCES '+dbSchema+'.ProgramYear(ProgramYearID);')
connProd.commit()
