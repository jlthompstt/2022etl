# -*- coding: utf-8 -*-
"""
Created on Fri Jan 28 14:58:35 2022

@author: JESSICA.THOMPSON
This script uploads the prepared UtilityInputs table to the TX EMV PUCT database. 
Prior to running, the Excel CreateUtilityInputs template must be completed. 
Prior to running, update loadDataDir, fileName, and dbSchema with values for this PY
The first time this is run, WACC and Bonus will be null. 
When EECRFs are filed, they will get updated using the same template and this script.
"""

#%% set data directory and variables

import pandas as pd 
import pyodbc 
from sqlalchemy import create_engine
import datetime

loadDataDir = "F://Texas PUC 2021-2024/EMV Database/2022etl/data/"
fileName = 'CreateUtilityInputs.xlsx'
dbSchema = 'TX2022'

#%% read in .xlsx table data and prepare for upload 

utilityInputs = pd.read_excel(loadDataDir+fileName,sheet_name='UtilityInputs')

colsKeep = ['UtilityID','ProgramYear','Bonus','WACC','ERCOT',
            'EMVCostAllocation','Goal_MW','Goal_MWh','AdminCosts',
            'RDCosts','EMVCosts','UCT_kWhRate',
            'UCT_kWRate','SIR_DiscountRate','LineLoss','AvoidedCostEscalator',
            'EMVBudget','CY1Allocation','CY2Allocation']

# remove extra rows and keep required columns
utilityInputs = utilityInputs.loc[~utilityInputs['UtilityID'].isnull(),colsKeep]

# convert relevant columns to numeric
utilityInputs['Goal_MW'] = utilityInputs['Goal_MW'].apply(lambda x: x.replace(',','').replace('$','') if isinstance(x,str) else x)
utilityInputs['Goal_MWh'] = utilityInputs['Goal_MWh'].apply(lambda x: x.replace(',','').replace('$','') if isinstance(x,str) else x)
utilityInputs['AdminCosts'] = utilityInputs['AdminCosts'].apply(lambda x: x.replace(',','').replace('$','') if isinstance(x,str) else x)
utilityInputs['RDCosts'] = utilityInputs['RDCosts'].apply(lambda x: x.replace(',','').replace('$','') if isinstance(x,str) else x)
utilityInputs['EMVCosts'] = utilityInputs['EMVCosts'].apply(lambda x: x.replace(',','').replace('$','') if isinstance(x,str) else x)
utilityInputs['Goal_MW'] = pd.to_numeric(utilityInputs['Goal_MW'],errors='coerce')
utilityInputs['Goal_MWh'] = pd.to_numeric(utilityInputs['Goal_MWh'],errors='coerce')
utilityInputs['AdminCosts'] = pd.to_numeric(utilityInputs['AdminCosts'],errors='coerce')
utilityInputs['RDCosts'] = pd.to_numeric(utilityInputs['RDCosts'],errors='coerce')
utilityInputs['EMVCosts'] = pd.to_numeric(utilityInputs['EMVCosts'],errors='coerce')


#%% check to see if data exists in current ProgramYear table and match w/ template data 

# open connection
conn = pyodbc.connect('Driver={SQL Server};Server=etss704sqldevqa,50179;Database=TX_EMV_PUCT;Trusted_Connection=yes;')

cursor = conn.cursor()
# pull matches and make sure they exist 
sql_statement = """
select UtilityID, ProgramYear, Bonus, WACC, ERCOT, 
EMVCostAllocation, Goal_MW, Goal_MWh, AdminCosts, 
RDCosts, EMVCosts, UCT_kWhRate, 
UCT_kWRate, SIR_DiscountRate, LineLoss, AvoidedCostEscalator, 
EMVBudget, CY1Allocation, CY2Allocation 
from 
"""
existingUtilities = pd.read_sql(sql_statement+dbSchema+'.UtilityInputs', conn)

utilitiesExisting = utilityInputs.merge(
    existingUtilities[['UtilityID','ProgramYear']],
    on=['UtilityID','ProgramYear'],how='left',indicator=True)

#%% divide out into updates and inserts 

# inserts 
utilityInserts = utilitiesExisting.loc[utilitiesExisting['_merge']=='left_only',colsKeep]
print('prepared '+str(len(utilityInserts))+' records for insertion')

# updates 
utilityUpdates = utilitiesExisting.loc[utilitiesExisting['_merge']!='left_only',]
print('prepared '+str(len(utilityUpdates))+' records for update')


#%% write updates and inserts to database 
# create sqlalchemy engine
con_str = 'mssql+pyodbc://etss704sqldevqa:50179/'+'TX_EMV_PUCT'+'?trusted_connection=yes&driver=SQL Server Native Client 11.0'
engine = create_engine(con_str, fast_executemany=True)

# inserts 
if len(utilityInserts)>0:
    print(datetime.datetime.now())
    utilityInserts.to_sql('UtilityInputs', con = engine, index=False, schema=dbSchema, if_exists='append')
    print(datetime.datetime.now())
    print(str(len(utilityInserts))+' rows inserted to '+dbSchema+'.UtilityInputs')
    conn.commit()

if len(utilityUpdates)>0:
    # updates 
    delIDs = utilityUpdates[['UtilityID']]
    delIDs.to_sql('tempDeletes', con = engine, index=False, schema=dbSchema, if_exists='replace')
    queryText = 'delete from '+dbSchema+'.UtilityInputs where UtilityID in (select UtilityID from '+dbSchema+'.tempDeletes);'
    cursor.execute(queryText)
    conn.commit()
    cursor.execute('drop table '+dbSchema+'.tempDeletes;')
    conn.commit()
    print('deleted '+str(len(utilityUpdates))+' records')
    # remove extra columns 
    utilityUpdates = utilityUpdates.drop(columns=['_merge'])
    utilityUpdates.to_sql('UtilityInputs', con = engine, index=False, schema=dbSchema, if_exists='append')
    print(str(len(utilityUpdates))+' rows updated in '+dbSchema+'.UtilityInputs')
    conn.commit()

