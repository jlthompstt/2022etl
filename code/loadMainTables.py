# -*- coding: utf-8 -*-
"""
Created on Wed Mar  9 16:24:19 2022

@author: JESSICA.THOMPSON
This script writes data from tracking tables to main tables. 
Will be run each quarter, after loadTrackingData and uploadPyidLookup.
"""
#%% set variables used for script 

import pandas as pd 
import pyodbc 

programYear = 2022
dbSchema = 'TX2022'
dataDir = "F://Texas PUC 2021-2024/EMV Database/2022etl/data/"

conn = pyodbc.connect('Driver={SQL Server};Server=etss704sqldevqa,50179;Database=TX_EMV_PUCT;Trusted_Connection=yes;')
cursor = conn.cursor()

trackingTables = ['epeApplianceTracking','CLEAResultTracking','centerPointTracking',
                  'frontierTracking','oncorTracking','xcelHomeLightingTracking']

#%% first drop any existing foreign key constraints 

# staging 
cursor.execute('ALTER TABLE '+dbSchema+'.ParticipantImpact drop CONSTRAINT FK_ParticipantImpact_Measure'+dbSchema+';')
cursor.execute('ALTER TABLE '+dbSchema+'.ParticipantImpact drop CONSTRAINT FK_ParticipantImpact_Participant'+dbSchema+';')
cursor.execute('ALTER TABLE '+dbSchema+'.Participant drop CONSTRAINT FK_Participant_ProgramYear'+dbSchema+';')
cursor.execute('ALTER TABLE '+dbSchema+'.Measure drop CONSTRAINT FK_Measure_ProgramYear'+dbSchema+';')
conn.commit()

#%% first load Measure

# match MeasureID to tracking tables where relevant
for i in trackingTables:
    print(i)
    queryText1 = 'update '+dbSchema+'.'+i+' set MeasureID=m.measure_id '
    queryText2 = 'from '+dbSchema+'.'+i+' t inner join '+dbSchema+'.Measure m '
    queryText3 = """
    on t.ProgramYearID=m.program_year_id and t.eul=m.eul 
    and t.source_measure_code=m.source_measure_code 
    and t.measure_description=m.measure_description 
    and coalesce(t.source_measure_id,0)=coalesce(m.source_measure_id,0)
    """
    cursor.execute(queryText1+queryText2+queryText3)
    conn.commit()

# update columns in main tables for those that match 
for i in trackingTables: 
    print(i)
    subTable = """
    (select distinct MeasureID, ProgramYearID, measure_description, 
    measure_reporting_description, quantity_unit, source_measure_code, 
    eul, source_measure_id from 
    """
    queryText = """ 
    on source.MeasureID=target.measure_id 
    when matched then update 
    set target.program_year_id=source.ProgramYearID, 
    target.measure_description=source.measure_description, 
    target.measure_reporting_description=source.measure_reporting_description, 
    target.quantity_unit=source.quantity_unit, 
    target.source_measure_code=source.source_measure_code, 
    target.eul=source.eul, target.source_measure_id=source.source_measure_id;
    """
    cursor.execute('merge '+dbSchema+'.Measure as target using '+subTable+dbSchema+'.'+i+') as source'+queryText)
    conn.commit()

# for unmatched, load records to Measure
# get ProgramYearID, MeasureDescription, MeasureReportingDescription, QuantityUnit, SourceMeasureCode, EUL from tracking
# join with MeasureDescriptionCategoryLookup to get initial MeasureCategoryID
for i in trackingTables:
    print(i)
    sqlQuery = """
    select distinct m.ProgramYearID as program_year_id, 
    case when l.MeasureCategoryID is null then 1 else l.MeasureCategoryID end as measure_category_id, 
    m.measure_description, m.measure_reporting_description, m.quantity_unit, m.source_measure_code, 
    m.eul, m.source_measure_id, 1 as realization_rate_kw, 1 as realization_rate_kwh 
    from 
    """
    sqlQuery2 = """
    m left join TX_EMV_PUCT.lookup.MeasureDescriptionCategoryLookup l 
    on m.measure_description=l.MeasureDescription 
    where m.MeasureID is null 
    and m.ProgramYearID is not null
    """
    cursor.execute('insert into '+dbSchema+'.Measure '+sqlQuery+dbSchema+'.'+i+sqlQuery2)
    conn.commit()
    
# re-match MeasureID to tracking tables where relevant
for i in trackingTables:
    queryText1 = 'update '+dbSchema+'.'+i+' set MeasureID=m.measure_id '
    queryText2 = 'from '+dbSchema+'.'+i+' t inner join '+dbSchema+'.Measure m '
    queryText3 = """
    on t.ProgramYearID=m.program_year_id and t.eul=m.eul 
    and t.source_measure_code=m.source_measure_code 
    and t.measure_description=m.measure_description 
    and coalesce(t.source_measure_id,0)=coalesce(m.source_measure_id,0)
    """
    cursor.execute(queryText1+queryText2+queryText3)
    conn.commit()
    queryText = 'select ProgramYearID, measure_description, source_measure_code, eul, source_measure_id, count(*) as cntRecords from '+dbSchema+'.'+i+' where MeasureID is null group by ProgramYearID, measure_description, source_measure_code, eul, source_measure_id;'
    missingPY = pd.read_sql(queryText, conn)
    if len(missingPY)>0:
        print('The following programs are missing MeasureIDs in '+i)
        print(missingPY)


#%% then Trade Ally

# add row to Trade Ally so each utility won't add one 
queryText = """
(trade_ally_name) values('NO TRADE ALLY')
"""
cursor.execute('insert into '+dbSchema+'.TradeAlly'+queryText)
conn.commit()

# match TradeAllyID to tracking tables where relevant
for i in trackingTables:
    print(i)
    queryText1 = 'update '+dbSchema+'.'+i+' set TradeAllyID=t.trade_ally_id '
    queryText2 = 'from '+dbSchema+'.'+i+' tr inner join '+dbSchema+'.TradeAlly t '
    queryText3 = """
    on tr.trade_ally_name=t.trade_ally_name 
    and coalesce(CAST(tr.trade_ally_address1 as varchar(255)),'NA')=coalesce(t.trade_ally_address1,'NA')
    and coalesce(CAST(tr.trade_ally_city as varchar),'NA')=coalesce(t.trade_ally_city,'NA')
    and coalesce(CAST(tr.trade_ally_state as varchar),'NA')=coalesce(t.trade_ally_state,'NA')
    and coalesce(CAST(tr.trade_ally_zip as varchar),'NA')=coalesce(t.trade_ally_zip,'NA') 
    and coalesce(CAST(tr.trade_ally_address2 as varchar),'NA')=coalesce(t.trade_ally_address2,'NA') 
    and coalesce(CAST(tr.trade_ally_contact_name as varchar),'NA')=coalesce(t.trade_ally_contact_name,'NA') 
    and coalesce(CAST(tr.trade_ally_email as varchar),'NA')=coalesce(t.trade_ally_email,'NA') 
    and coalesce(CAST(tr.trade_ally_phone as varchar),'NA')=coalesce(t.trade_ally_phone,'NA') 
    """
    cursor.execute(queryText1+queryText2+queryText3)
    conn.commit()
    
# for unmatched, load records to TradeAlly
for i in trackingTables:
    print(i)
    sqlQuery = """
    select distinct trade_ally_name, trade_ally_contact_name, trade_ally_email, 
    trade_ally_phone, trade_ally_address1, trade_ally_address2, trade_ally_city, 
    trade_ally_state, trade_ally_zip 
    from 
    """
    sqlQuery2 = """ 
    where TradeAllyID is null 
    """
    cursor.execute('insert into '+dbSchema+'.TradeAlly '+sqlQuery+dbSchema+'.'+i+sqlQuery2)
    conn.commit()

# re-match TradeAllyIDs to tracking tables where relevant
for i in trackingTables:
    print(i)
    queryText1 = 'update '+dbSchema+'.'+i+' set TradeAllyID=t.trade_ally_id '
    queryText2 = 'from '+dbSchema+'.'+i+' tr inner join '+dbSchema+'.TradeAlly t '
    queryText3 = """
    on tr.trade_ally_name=t.trade_ally_name 
    and coalesce(CAST(tr.trade_ally_address1 as varchar(255)),'NA')=coalesce(t.trade_ally_address1,'NA')
    and coalesce(CAST(tr.trade_ally_city as varchar),'NA')=coalesce(t.trade_ally_city,'NA')
    and coalesce(CAST(tr.trade_ally_state as varchar),'NA')=coalesce(t.trade_ally_state,'NA')
    and coalesce(CAST(tr.trade_ally_zip as varchar),'NA')=coalesce(t.trade_ally_zip,'NA')
    """
    cursor.execute(queryText1+queryText2+queryText3)
    conn.commit()
    queryText = 'select ProgramYearID, trade_ally_name, trade_ally_contact_name, trade_ally_email, trade_ally_address1, count(*) as cntRecords from '+dbSchema+'.'+i+' where TradeAllyID is null group by ProgramYearID, trade_ally_name, trade_ally_contact_name, trade_ally_email, trade_ally_address1;'
    missingPY = pd.read_sql(queryText, conn)
    if len(missingPY)>0:
        print('The following programs are missing TradeAllyIDs in '+i)
        print(missingPY)

#%% then Participant 

# match ParticipantID to tracking tables where relevant
for i in trackingTables:
    print(i)
    queryText1 = 'update '+dbSchema+'.'+i+' set ParticipantID=t.participant_id '
    queryText2 = 'from '+dbSchema+'.'+i+' tr inner join '+dbSchema+'.Participant t '
    queryText3 = """
    on tr.ProgramYearID=t.program_year_id  
    and tr.participant_code=t.participant_code
    and tr.participant_code_source=t.participant_code_source 
    and coalesce(tr.esiid,'NA')=coalesce(t.esiid,'NA') 
    and coalesce(CAST(tr.premise as varchar),'NA')=coalesce(t.premise,'NA') 
    and coalesce(tr.customer_name,'NA')=coalesce(t.customer_name,'NA') 
    and coalesce(CAST(tr.mail_address1 as varchar),'NA')=coalesce(t.mail_address1,'NA') 
    and coalesce(CAST(tr.mail_city as varchar),'NA')=coalesce(t.mail_city,'NA') 
    and coalesce(CAST(tr.mail_zip_code as varchar),'NA')=coalesce(t.mail_zip_code,'NA') 
    """
    cursor.execute(queryText1+queryText2+queryText3)
    conn.commit()
    
# for unmatched, load records to Participant
for i in trackingTables:
    print(i)
    sqlQuery = """
    select distinct participant_code, participant_code_source, ProgramYearID as program_year_id, 
    esiid, premise, customer_name, mail_address1, mail_address2, 
    mail_city as varchar, 
    'TX' as mail_state, mail_zip_code, 
    email_address, primary_phone as varchar
    from 
    """
    sqlQuery2 = """ 
    where ParticipantID is null and participant_code is not null 
    """
    cursor.execute('insert into '+dbSchema+'.Participant '+sqlQuery+dbSchema+'.'+i+sqlQuery2)
    conn.commit()
    
# re-match ParticipantIDs to tracking tables where relevant
for i in trackingTables:
    print(i)
    queryText1 = 'update '+dbSchema+'.'+i+' set ParticipantID=t.participant_id '
    queryText2 = 'from '+dbSchema+'.'+i+' tr inner join '+dbSchema+'.Participant t '
    queryText3 = """
    on tr.ProgramYearID=t.program_year_id  
    and CAST(tr.participant_code as varchar(255))=t.participant_code
    and CAST(tr.participant_code_source as varchar(255))=t.participant_code_source 
    and coalesce(CAST(tr.esiid as varchar(255)),'NA')=coalesce(t.esiid,'NA') 
    and coalesce(CAST(tr.premise as varchar(255)),'NA')=coalesce(t.premise,'NA') 
    and coalesce(CAST(tr.customer_name as varchar(255)),'NA')=coalesce(t.customer_name,'NA') 
    and coalesce(CAST(tr.mail_address1 as varchar(255)),'NA')=coalesce(t.mail_address1,'NA') 
    and coalesce(CAST(tr.mail_city as varchar(255)),'NA')=coalesce(t.mail_city,'NA') 
    and coalesce(CAST(tr.mail_zip_code as varchar(255)),'NA')=coalesce(t.mail_zip_code,'NA') 
    """
    cursor.execute(queryText1+queryText2+queryText3)
    conn.commit()
    queryText = 'select ProgramYearID, participant_code, participant_code_source, esiid, premise, customer_name, count(*) as cntRecords from '+dbSchema+'.'+i+' where ParticipantID is null group by ProgramYearID, participant_code, participant_code_source, esiid, premise, customer_name;'
    missingPY = pd.read_sql(queryText, conn)
    if len(missingPY)>0:
        print('The following programs are missing ParticipantIDs in '+i)
        print(missingPY)

#%% then Participant Impact

# match ParticipantImpcatID to tracking tables where relevant
for i in trackingTables:
    print(i)
    queryText1 = 'update '+dbSchema+'.'+i+' set ParticipantImpactID=t.participant_impact_id '
    queryText2 = 'from '+dbSchema+'.'+i+' tr inner join '+dbSchema+'.ParticipantImpact t '
    queryText3 = """
    on tr.ParticipantID=t.participant_id  
    and tr.SourceDataRow = t.source_data_row_id 
    and tr.MeasureID = t.measure_id 
    and tr.TradeAllyID = t.trade_ally_id 
    """
    cursor.execute(queryText1+queryText2+queryText3)
    conn.commit()
    
# update columns in main tables for those that match 
for i in trackingTables: 
    print(i)
    subTable = """(select distinct SourceDataRow, ParticipantID, MeasureID, TradeAllyID, 
    participant_impact_date, count, CAST(replace(quantity,',','') as float) as quantity, incentive, 
    exante_savings_kw, exante_savings_kwh, project_id, 
    CAST(measure_calculation_id as varchar) as measure_calculation_id from 
    """
    queryText = """ 
    on source.MeasureID=target.measure_id 
    and source.SourceDataRow=target.source_data_row_id 
    and source.ParticipantID=target.participant_id 
    and source.TradeAllyID=target.trade_ally_id 
    when matched then update 
    set target.participant_impact_date=source.participant_impact_date, 
    target.count=source.count, 
    target.quantity=CAST(replace(source.quantity,',','') as float), 
    target.incentive=source.incentive, 
    target.exante_savings_kw=source.exante_savings_kw, 
    target.exante_savings_kwh=source.exante_savings_kwh, 
    target.measure_calculation_id=CAST(source.measure_calculation_id as varchar), 
    target.project_id=CAST(source.project_id as varchar);
    """
    cursor.execute('merge '+dbSchema+'.ParticipantImpact as target using '+subTable+dbSchema+'.'+i+') as source'+queryText)
    conn.commit()
    
# for unmatched, load records to ParticipantImpact
for i in trackingTables:
    print(i)
    sqlQuery = """
    select distinct SourceDataRow as source_data_row_id, ParticipantID as participant_id, 
    MeasureID as measure_id, participant_impact_date, 
    count, CAST(replace(quantity,',','') as float), incentive, exante_savings_kw, exante_savings_kwh,  
    CAST(measure_calculation_id as varchar), TradeAllyID as trade_ally_id, 
    CAST(project_id as varchar)  
    from 
    """
    sqlQuery2 = """ 
    where ParticipantImpactID is null 
    """
    cursor.execute('insert into '+dbSchema+'.ParticipantImpact '+sqlQuery+dbSchema+'.'+i+sqlQuery2)
    conn.commit()
    
# re-match ParticipantImpactIDs to tracking tables where relevant
for i in trackingTables:
    print(i)
    queryText1 = 'update '+dbSchema+'.'+i+' set ParticipantImpactID=t.participant_impact_id '
    queryText2 = 'from '+dbSchema+'.'+i+' tr inner join '+dbSchema+'.ParticipantImpact t '
    queryText3 = """
    on tr.ParticipantID=t.participant_id  
    and tr.SourceDataRow = t.source_data_row_id 
    and tr.MeasureID = t.measure_id 
    and tr.TradeAllyID = t.trade_ally_id 
    """
    cursor.execute(queryText1+queryText2+queryText3)
    conn.commit()
    queryText = 'select ProgramYearID, MeasureID, ParticipantID, TradeAllyID, count(*) as cntRecords from '+dbSchema+'.'+i+' where ParticipantImpactID is null group by ProgramYearID, MeasureID, ParticipantID, TradeAllyID;'
    missingPY = pd.read_sql(queryText, conn)
    if len(missingPY)>0:
        print('The following programs are missing ParticipantImpactIDs in '+i)
        print(missingPY)
        
#%% create CustomerImpactExport view 

queryText = """
.CustomerImpactExport as 
SELECT pi.participant_impact_id, p.participant_id, p.participant_code, p.esiid, p.participant_code_source, 
                         p.customer_name, p.mail_address1, p.mail_address2, p.mail_city, p.mail_state, 
                         p.mail_zip_code, p.email_address, p.primary_phone, p.program_year_id, py.UtilityName, 
                         py.LeadEvaluator, py.ProgramYear, py.UtilityProgramName, py.ProgramName, py.ProgramDescription, 
                         py.SectorName, py.ReportSectorDescription, py.ProgramTypeName, py.ProgramGroupName, 
                         m.measure_description, m.measure_reporting_description, m.source_measure_code, m.realization_rate_kw, 
                         m.realization_rate_kwh, mc.MeasureCategoryName, pi.project_id, pi.source_data_row_id, pi.participant_impact_date, pi.count, 
                         pi.quantity, pi.incentive, pi.exante_savings_kw, pi.exante_savings_kwh, t.trade_ally_id, t.trade_ally_name, 
                         t.trade_ally_contact_name, t.trade_ally_email, t.trade_ally_phone, t.trade_ally_address1, t.trade_ally_address2, 
                         t.trade_ally_city, t.trade_ally_state, t.trade_ally_zip, py.UtilityID, py.ProgramID, py.SectorID, py.ProgramTypeID, 
                         py.ProgramGroupID, m.measure_id, m.quantity_unit, m.measure_category_id, py.PrecisionRankName, py.ReportSectorSort, 
                         m.eul, py.EvaluationPriority, py.ReportSectorID
FROM            TX2022.Participant p INNER JOIN
                         TX2022.ParticipantImpact AS pi ON p.participant_id = pi.participant_id INNER JOIN
                         TX2022.Measure AS m ON pi.measure_id = m.measure_id INNER JOIN
                         lookup.MeasureCategory AS mc ON m.measure_category_id = mc.MeasureCategoryID INNER JOIN
                         TX2022.ProgramYearList AS py ON m.program_year_id = py.ProgramYearID INNER JOIN 
                         TX2022.TradeAlly AS t ON pi.trade_ally_id = t.trade_ally_id;
"""

# check if exists 
sql_query = """
select
    count(*)
from
    INFORMATION_SCHEMA.VIEWS
where
    table_name = 'CustomerImpactExport'
    and table_schema = ?
"""

cursor.execute(sql_query, dbSchema)
results = cursor.fetchall()

if results[0][0]>0:
    sql_statement = 'alter view '+dbSchema+queryText
    cursor.execute(sql_statement)
    conn.commit()
else:
    sql_statement = 'create view '+dbSchema+queryText
    cursor.execute(sql_statement)
    conn.commit()

#%% re-add previously dropped foreign key constraints 

# staging 
cursor.execute('ALTER TABLE '+dbSchema+'.ParticipantImpact ADD CONSTRAINT FK_ParticipantImpact_Measure'+dbSchema+' FOREIGN KEY (measure_id) REFERENCES '+dbSchema+'.Measure(measure_id);')
cursor.execute('ALTER TABLE '+dbSchema+'.ParticipantImpact ADD CONSTRAINT FK_ParticipantImpact_Participant'+dbSchema+' FOREIGN KEY (participant_id) REFERENCES '+dbSchema+'.Participant(participant_id);')
cursor.execute('ALTER TABLE '+dbSchema+'.Participant ADD CONSTRAINT FK_Participant_ProgramYear'+dbSchema+' FOREIGN KEY (program_year_id) REFERENCES '+dbSchema+'.ProgramYear(ProgramYearID);')
cursor.execute('ALTER TABLE '+dbSchema+'.Measure ADD CONSTRAINT FK_Measure_ProgramYear'+dbSchema+' FOREIGN KEY (program_year_id) REFERENCES '+dbSchema+'.ProgramYear(ProgramYearID);')
conn.commit()

#%% check for any records in main tables not present in tracking tables 

sqlQuery = """
select participant_impact_id from TX2022.CustomerImpactExport 
except (select ParticipantImpactID as participant_impact_id from TX2022.trackingSummary)
"""

extraRecords = pd.read_sql(sqlQuery,conn)

print('Main tables contain '+str(len(extraRecords))+' records not found in tracking tables')

#%% delete any extras in CIE found above 

sqlQuery = """
delete from TX2022.ParticipantImpact where participant_impact_id in (select participant_impact_id from TX2022.CustomerImpactExport 
except (select ParticipantImpactID as participant_impact_id from TX2022.trackingSummary))
"""

cursor.execute(sqlQuery)
conn.commit()

print(str(len(extraRecords))+' unmatched records deleted from main tables')

#%% check for any records in tracking tables not present in main tables 

sqlQuery = """
select ParticipantImpactID from TX2022.trackingSummary 
except (select participant_impact_id as ParticipantImpactID from TX2022.CustomerImpactExport)
"""

extraRecords = pd.read_sql(sqlQuery,conn)

print('Tracking tables contain '+str(len(extraRecords))+' records not found in main tables')

#%% check tracking tables against main tables 

sqlQuery = """
select ProgramYearID, Utility, 
Sector, count(distinct participant_code) as TrackingParts,
count(*) as TrackingRecords, sum(incentive) as TrackIncentive, 
sum(exante_savings_kw) as TrackkW_savings,
sum(exante_savings_kwh) as TrackkWh_savings 
from TX2022.trackingSummary 
group by ProgramYearID, Utility, Sector
"""

currentTracking = pd.read_sql(sqlQuery,conn)

sqlQuery = """
select program_year_id as ProgramYearID, UtilityName, UtilityProgramName, SectorName, 
count(distinct participant_id) as cntParts, 
count(*) as cntRecords, sum(incentive) as sumIncentive, 
sum(exante_savings_kw) as sumkW_savings, 
sum(exante_savings_kwh) as sumkWh_savings 
from TX2022.CustomerImpactExport 
group by program_year_id, UtilityName, UtilityProgramName, SectorName 
"""

currentMain = pd.read_sql(sqlQuery,conn)

checkData = currentTracking.merge(currentMain,how='outer',on='ProgramYearID')

#%% check tracking table programs against EEPRs programs 

sqlQuery = """
select ProgramYearID, Utility, UtilityProgramName as trackingUtilityProgramName, 
Sector, data_provider, 
count(*) as cntRecords, sum(incentive) as sumIncentive, 
sum(exante_savings_kw) as sumExAnteSavingskW,
sum(exante_savings_kwh) as sumExAnteSavingskWh 
from TX2022.trackingSummary 
group by ProgramYearID, Utility, UtilityProgramName, Sector, data_provider 
"""

currentTracking = pd.read_sql(sqlQuery,conn)

sqlQuery = """
select ProgramYearID, UtilityName, UtilityProgramName, ProgramName, 
SectorName, ReportSectorDescription, ProgramGroupName, ProgramTypeName 
from TX2022.ProgramYearList 
"""

programYearList = pd.read_sql(sqlQuery,conn)

checkPrograms = currentTracking.merge(programYearList,how='outer',on='ProgramYearID')

#%% output checking data 
# output checks
writer = pd.ExcelWriter(dataDir+'trackingDataCheck.xlsx', engine='xlsxwriter')

checkData.sort_values(by=['ProgramYearID']).to_excel(
    writer, sheet_name='trackingMainCheck', index=False)
checkPrograms.sort_values(by=['ProgramYearID','Sector',
                              'trackingUtilityProgramName']).to_excel(
                                  writer, sheet_name='trackingEEPRmatch',index=False)

writer.save()

