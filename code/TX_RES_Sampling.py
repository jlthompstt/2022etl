#%% -*- coding: utf-8 -*-
"""
Created on Wed July 05 15:25:04 2022

@author: SIMRAN.PADAM
"""
from re import T, X
from sys import displayhook
import pandas as pd
import pyodbc
import numpy as np
import os
from datetime import datetime
from sqlalchemy import create_engine
from pathlib import Path
import glob
from IPython.display import display
from openpyxl import load_workbook
from collections import Counter
import itertools
from math import nan, isnan

data_directory = "Data"
conn = pyodbc.connect('''Driver={SQL Server};
                      Server=etss704sqldevqa\ETSS704SQL12_QA;
                      Database=TX_EMV_PUCT;
                      Trusted_Connection=yes;''')
output_folder = 'F:\Texas PUC 2021-2024\Sample\PY2022\Residential\\'
cursor = conn.cursor()
con_str = ('mssql+pyodbc://etss704sqldevqa\ETSS704SQL12_QA/' + 'TX_EMV_PUCT' + '?trusted_connection=yes&driver=SQL Server Native Client 11.0')
engine = create_engine(con_str, fast_executemany=True)

#%% Sampling for AEP Texas

#List of programs to iterate through the sampling
programs = ['Hard-to-Reach SOP','Residential SOP','SMART Source Solar PV MTP','Targeted Low-Income Energy Efficiency Program']

# For loop to iterate through the above programs, creating an initial population df, characterized, choosing samples and sample details. Then writing each df to an excel sheet
for program in programs:
    # query to get inital population 
    
    x = f'''
    SELECT UtilityProgramName, participant_id, participant_code, esiid, customer_name, mail_address1,  program_year_id, UtilityName, ProgramYear, UtilityProgramName
    , ProgramName, ProgramDescription, SectorName, ReportSectorDescription, ProgramTypeName, ProgramGroupName, measure_description, measure_reporting_description
    , source_measure_code, realization_rate_kw, realization_rate_kwh, MeasureCategoryName, project_id, source_data_row_id, participant_impact_date, count, quantity
    , incentive, exante_savings_kw, exante_savings_kwh,trade_ally_zip, UtilityID, ProgramID, SectorID, ProgramTypeID
    , ProgramGroupID, measure_id, quantity_unit, measure_category_id, PrecisionRankName, ReportSectorSort, eul, EvaluationPriority, ReportSectorID
    FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
    where UtilityName = 'AEP Texas' ;
    '''
    
    # Running the above queries in SQL and assiging it to Y and B variable DF
    y = pd.read_sql(x, conn)
    
    
    # Writer variable for writer attribute when using to.excel 
    z =pd.ExcelWriter(output_folder + f"Aep_Texas\AEP_{program}.xlsx")
    
    # writing the characterized and initial population df to each files sheet in excel
    y.to_excel(z, sheet_name='Population', index=False)
    
    #iterate through programs to get samples sheet, population to choose samples from and samples attribute sheet. d variable: sample df - t variable: samples with attributes
    if program == 'Hard-to-Reach SOP':
        a = f'''
        SELECT esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}' and UtilityName = 'AEP Texas' and (measure_description = 'R-AirInf' OR measure_description = 'R-AirPurifier' OR measure_description = 'R-CeilingIns'
        OR measure_description = 'R-DuctEff' OR measure_description = 'R-DuctEffAlt')
        GROUP BY esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        a2 = f'''
        SELECT esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}' and UtilityName = 'AEP Texas' and (measure_description = 'R-AirInf' OR measure_description = 'R-CeilingIns'
        OR measure_description = 'R-DuctEff' OR measure_description = 'R-DuctEffAlt')
        GROUP BY esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        b = pd.read_sql(a, conn)
        b2 = pd.read_sql(a2, conn)
        b.to_excel(z, sheet_name='HTR_Characterized_DR', index=False)
        b2.to_excel(z, sheet_name='HTR_Characterized_OS', index=False)
        ddr = b.sample(n=1)
        dos = b2.sample(n=24)
        account_list_dr = ddr['esiid'].tolist()
        account_list_os = dos['esiid'].tolist()
        tdr = y[y['esiid'].isin(account_list_dr)]
        tos = y[y['esiid'].isin(account_list_os)]
        ddr.to_excel(z,sheet_name=f'{program}_DR', index=False)
        dos.to_excel(z,sheet_name=f'{program}_OS', index=False)
        tdr.to_excel(z,sheet_name=f'{program}_Attributes_DR', index=False)
        tos.to_excel(z,sheet_name=f'{program}_Attributes_OS', index=False)
        z.save()
    if program == 'Residential SOP':
        a = f'''
        SELECT esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}' and UtilityName = 'AEP Texas' and (measure_description = 'R-MSHP' OR measure_description = 'R-AirPurifier' OR measure_description = 'R-CeilingIns' OR measure_description = 'R-CentAC' 
        OR measure_description = 'R-CentHP' OR  measure_description = 'R-DuctEffR' or measure_description = 'R-DuctEffAlt')
        GROUP BY esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        a2 = f'''
        SELECT esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}' and UtilityName = 'AEP Texas' and (measure_description = 'R-MSHP' OR measure_description = 'R-CeilingIns' OR measure_description = 'R-CentAC' 
        OR measure_description = 'R-CentHP' OR  measure_description = 'R-DuctEffR' or measure_description = 'R-DuctEffAlt')
        GROUP BY esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        b = pd.read_sql(a, conn)
        b.to_excel(z, sheet_name='RSOP_Characterized_DR', index=False)
        b2 = pd.read_sql(a2, conn)
        b2.to_excel(z, sheet_name='RSOP_Characterized_OS', index=False)
        ddr = b.sample(n=1)
        dos = b2.sample(n=32)
        account_list_dr = ddr['esiid'].tolist()
        account_list_os = dos['esiid'].tolist()
        tdr = y[y['esiid'].isin(account_list_dr)]
        tos = y[y['esiid'].isin(account_list_os)]
        ddr.to_excel(z,sheet_name=f'{program}_DR', index=False)
        dos.to_excel(z,sheet_name=f'{program}_OS', index=False)
        tdr.to_excel(z,sheet_name=f'{program}_Attributes_DR', index=False)
        tos.to_excel(z,sheet_name=f'{program}_Attributes_OS', index=False)
        z.save()
    if program == 'SMART Source Solar PV MTP':
        a = f'''
        SELECT esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}' and UtilityName = 'AEP Texas' and (measure_description = 'R-SolarPV')
        GROUP BY esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        b = pd.read_sql(a, conn)
        b.to_excel(z, sheet_name='SmartSource_Characterized', index=False)
        ddr = b.sample(n=2)
        dos = b.sample(n=16)
        account_list_dr = ddr['esiid'].tolist()
        account_list_os = dos['esiid'].tolist()
        tdr = y[y['esiid'].isin(account_list_dr)]
        tos = y[y['esiid'].isin(account_list_os)]
        ddr.to_excel(z,sheet_name='SmartSource_DR', index=False)
        dos.to_excel(z,sheet_name='SmartSource_OS', index=False)
        tdr.to_excel(z,sheet_name='SmartSource_Attributes_DR', index=False)
        tos.to_excel(z,sheet_name='SmartSource_Attributes_OS', index=False)
        z.save()
    if program == 'Targeted Low-Income Energy Efficiency Program':
        a = f'''
        SELECT esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}' and UtilityName = 'AEP Texas' and (measure_description = 'R-CeilingsIns' OR measure_description = 'R-CentHP' OR measure_description = 'R-DuctEff')
        GROUP BY esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        b = pd.read_sql(a, conn)
        b.to_excel(z, sheet_name='LowIncome_Characterized', index=False)
        ddr = b.sample(n=1)
        dos = b.sample(n=16)
        account_list_dr = ddr['esiid'].tolist()
        account_list_os = dos['esiid'].tolist()
        tdr = y[y['esiid'].isin(account_list_dr)]
        tos = y[y['esiid'].isin(account_list_os)]
        ddr.to_excel(z,sheet_name='LowIncome_DR', index=False)
        dos.to_excel(z,sheet_name='LowIncome_OS', index=False)
        tdr.to_excel(z,sheet_name='LowIncome_Attributes_DR', index=False)
        tos.to_excel(z,sheet_name='LowIncome_Attributes_OS', index=False)
        z.save()

# %%
#%% Sampling for CenterPoint

#List of programs to iterate through the sampling
programs = ['Residential & Small Commercial Standard Offer Program', 'Targeted Low Income MTP (Agencies in Action)', 'Hard-to-Reach Standard Offer Program']

# For loop to iterate through the above programs, creating an initial population df, characterized, choosing samples and sample details. Then writing each df to an excel sheet
for program in programs:
    # query to get inital population 
    
    x = f'''
    SELECT UtilityProgramName, participant_id, participant_code, esiid, customer_name, mail_address1,  program_year_id, UtilityName, ProgramYear, UtilityProgramName
    , ProgramName, ProgramDescription, SectorName, ReportSectorDescription, ProgramTypeName, ProgramGroupName, measure_description, measure_reporting_description
    , source_measure_code, realization_rate_kw, realization_rate_kwh, MeasureCategoryName, project_id, source_data_row_id, participant_impact_date, count, quantity
    , incentive, exante_savings_kw, exante_savings_kwh,trade_ally_zip, UtilityID, ProgramID, SectorID, ProgramTypeID
    , ProgramGroupID, measure_id, quantity_unit, measure_category_id, PrecisionRankName, ReportSectorSort, eul, EvaluationPriority, ReportSectorID
    FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
    where UtilityName = 'CenterPoint' ;
    '''
    
    # Running the above queries in SQL and assiging it to Y and B variable DF
    y = pd.read_sql(x, conn)
    
    
    # Writer variable for writer attribute when using to.excel 
    z =pd.ExcelWriter(output_folder + f"CenterPoint\CP_{program}.xlsx")
    
    # writing the characterized and initial population df to each files sheet in excel
    y.to_excel(z, sheet_name='Population', index=False)
    
    #iterate through programs to get samples sheet, population to choose samples from and samples attribute sheet. d variable: sample df - t variable: samples with attributes
    if program == 'Residential & Small Commercial Standard Offer Program':
        a = f'''
        SELECT participant_id, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}' and UtilityName = 'CenterPoint' and (measure_description =  'Central Air Conditioner Replacement' OR measure_description =  'Central Heat Pump Replacement' 
        OR measure_description = 'Solar PV System')
        GROUP BY participant_id, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        b = pd.read_sql(a, conn)
        b.to_excel(z, sheet_name='Standard_Offer_Characterized', index=False)
        ddr = b.sample(n=2)
        dos = b.sample(n=32)
        account_list_dr = ddr['participant_id'].tolist()
        account_list_os = dos['participant_id'].tolist()
        tdr = y[y['participant_id'].isin(account_list_dr)]
        tos = y[y['participant_id'].isin(account_list_os)]
        ddr.to_excel(z,sheet_name='Standard_Offer_DR', index=False)
        dos.to_excel(z,sheet_name='Standard_Offer_OS', index=False)
        tdr.to_excel(z,sheet_name='Standard_Offer_Attributes_DR', index=False)
        tos.to_excel(z,sheet_name='Standard_Offer_Attributes_OS', index=False)
        z.save()
    if program == 'Targeted Low Income MTP (Agencies in Action)':
        a = f'''
        SELECT participant_id, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}' and UtilityName = 'CenterPoint' and (measure_description =  'Air Infiltration' OR measure_description =  'Ceiling Insulation' 
        OR measure_description = 'Central Heat Pump Replacement' OR measure_description = 'Multi-Family HP Unit')
        GROUP BY participant_id, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        b = pd.read_sql(a, conn)
        b.to_excel(z, sheet_name='Low_Income_Characterized', index=False)
        ddr = b.sample(n=1)
        dos = b.sample(n=16)
        account_list_dr = ddr['participant_id'].tolist()
        account_list_os = dos['participant_id'].tolist()
        tdr = y[y['participant_id'].isin(account_list_dr)]
        tos = y[y['participant_id'].isin(account_list_os)]
        ddr.to_excel(z,sheet_name='Low_Income_DR', index=False)
        dos.to_excel(z,sheet_name='Low_Income_OS', index=False)
        tdr.to_excel(z,sheet_name='Low_Income_Attributes_DR', index=False)
        tos.to_excel(z,sheet_name='Low_Income_Attributes_OS', index=False)
        z.save()
    if program == 'Hard-to-Reach Standard Offer Program':
        a = f'''
        SELECT participant_id, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}' and UtilityName = 'CenterPoint' and (measure_description =  'Ceiling Insulation')
        GROUP BY participant_id, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        b = pd.read_sql(a, conn)
        b.to_excel(z, sheet_name='HTR_Characterized', index=False)
        ddr = b.sample(n=1)
        dos = b.sample(n=16)
        account_list_dr = ddr['participant_id'].tolist()
        account_list_os = dos['participant_id'].tolist()
        tdr = y[y['participant_id'].isin(account_list_dr)]
        tos = y[y['participant_id'].isin(account_list_os)]
        ddr.to_excel(z,sheet_name='HTR_DR', index=False)
        dos.to_excel(z,sheet_name='HTR_OS', index=False)
        tdr.to_excel(z,sheet_name='HTR_Attributes_DR', index=False)
        tos.to_excel(z,sheet_name='HTR_Attributes_OS', index=False)
        z.save()
#%% Sampling for Entergy

#List of programs to iterate through the sampling
programs = ['Hard-to-Reach SOP', 'Residential SOP']

# For loop to iterate through the above programs, creating an initial population df, characterized, choosing samples and sample details. Then writing each df to an excel sheet
for program in programs:
    # query to get inital population 
    
    x = f'''
    SELECT UtilityProgramName, participant_id, participant_code, esiid, customer_name, mail_address1,  program_year_id, UtilityName, ProgramYear, UtilityProgramName
    , ProgramName, ProgramDescription, SectorName, ReportSectorDescription, ProgramTypeName, ProgramGroupName, measure_description, measure_reporting_description
    , source_measure_code, realization_rate_kw, realization_rate_kwh, MeasureCategoryName, project_id, source_data_row_id, participant_impact_date, count, quantity
    , incentive, exante_savings_kw, exante_savings_kwh,trade_ally_zip, UtilityID, ProgramID, SectorID, ProgramTypeID
    , ProgramGroupID, measure_id, quantity_unit, measure_category_id, PrecisionRankName, ReportSectorSort, eul, EvaluationPriority, ReportSectorID
    FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
    where UtilityName = 'Entergy' ;
    '''
    
    # Running the above queries in SQL and assiging it to Y and B variable DF
    y = pd.read_sql(x, conn)
    
    
    # Writer variable for writer attribute when using to.excel 
    z =pd.ExcelWriter(output_folder + f"Entergy\Entergy_{program}.xlsx")
    
    # writing the characterized and initial population df to each files sheet in excel
    y.to_excel(z, sheet_name='Population', index=False)
    
    #iterate through programs to get samples sheet, population to choose samples from and samples attribute sheet. d variable: sample df - t variable: samples with attributes
    if program == 'Hard-to-Reach SOP':
        a = f'''
        SELECT esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}' and UtilityName = 'Entergy' and (measure_description =  'R-AirInf' OR measure_description =  'R-DuctEff')
        GROUP BY esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        b = pd.read_sql(a, conn)
        b.to_excel(z, sheet_name=f'{program}_Characterized', index=False)
        ddr = b.sample(n=0)
        dos = b.sample(n=24)
        account_list_dr = ddr['esiid'].tolist()
        account_list_os = dos['esiid'].tolist()
        tdr = y[y['esiid'].isin(account_list_dr)]
        tos = y[y['esiid'].isin(account_list_os)]
        ddr.to_excel(z,sheet_name=f'{program}_DR', index=False)
        dos.to_excel(z,sheet_name=f'{program}_OS', index=False)
        tdr.to_excel(z,sheet_name=f'{program}_Attributes_DR', index=False)
        tos.to_excel(z,sheet_name=f'{program}_Attributes_OS', index=False)
        z.save()
    if program == 'Residential SOP':
        a = f'''
        SELECT esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}' and UtilityName = 'Entergy' and (measure_description =  'R-AirPurifier' OR measure_description =  'R-CeilingIns'
        OR measure_description =  'R-DuctEff')
        GROUP BY esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        b = pd.read_sql(a, conn)
        b.to_excel(z, sheet_name=f'{program}_Characterized', index=False)
        ddr = b.sample(n=2)
        dos = b.sample(n=16)
        account_list_dr = ddr['esiid'].tolist()
        account_list_os = dos['esiid'].tolist()
        tdr = y[y['esiid'].isin(account_list_dr)]
        tos = y[y['esiid'].isin(account_list_os)]
        ddr.to_excel(z,sheet_name=f'{program}_DR', index=False)
        dos.to_excel(z,sheet_name=f'{program}_OS', index=False)
        tdr.to_excel(z,sheet_name=f'{program}_Attributes_DR', index=False)
        tos.to_excel(z,sheet_name=f'{program}_Attributes_OS', index=False)
        z.save()
#%% Sampling for Oncor

#List of programs to iterate through the sampling
programs = ['Hard-to-Reach SOP','Home Energy Efficiency SOP','Solar PV SOP','Targeted Weatherization Low-Income SOP']

# For loop to iterate through the above programs, creating an initial population df, characterized, choosing samples and sample details. Then writing each df to an excel sheet
for program in programs:
    # query to get inital population 
    
    x = f'''
    SELECT UtilityProgramName, participant_id, participant_code, esiid, customer_name, mail_address1,  program_year_id, UtilityName, ProgramYear, UtilityProgramName
    , ProgramName, ProgramDescription, SectorName, ReportSectorDescription, ProgramTypeName, ProgramGroupName, measure_description, measure_reporting_description
    , source_measure_code, realization_rate_kw, realization_rate_kwh, MeasureCategoryName, project_id, source_data_row_id, participant_impact_date, count, quantity
    , incentive, exante_savings_kw, exante_savings_kwh,trade_ally_zip, UtilityID, ProgramID, SectorID, ProgramTypeID
    , ProgramGroupID, measure_id, quantity_unit, measure_category_id, PrecisionRankName, ReportSectorSort, eul, EvaluationPriority, ReportSectorID
    FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
    where UtilityName = 'Oncor' ;
    '''
    
    # Running the above queries in SQL and assiging it to Y and B variable DF
    y = pd.read_sql(x, conn)
    
    
    # Writer variable for writer attribute when using to.excel 
    z =pd.ExcelWriter(output_folder + f"Oncor\Oncor_{program}.xlsx")
    
    # writing the characterized and initial population df to each files sheet in excel
    y.to_excel(z, sheet_name='Population', index=False)
    
    #iterate through programs to get samples sheet, population to choose samples from and samples attribute sheet. d variable: sample df - t variable: samples with attributes
    if program == 'Hard-to-Reach SOP':
        a = f'''
        SELECT esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}' and UtilityName = 'Oncor' and ReportSectorDescription = 'Residential' and (measure_description = 'Air Infiltration' OR measure_description = 'Central Air Conditioner' 
        OR measure_description = 'Central Heat Pump')
        GROUP BY esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        b = pd.read_sql(a, conn)
        b.to_excel(z, sheet_name=f'{program}_Characterized', index=False)
        ddr = b.sample(n=0)
        dos = b.sample(n=24)
        account_list_dr = ddr['esiid'].tolist()
        account_list_os = dos['esiid'].tolist()
        tdr = y[y['esiid'].isin(account_list_dr)]
        tos = y[y['esiid'].isin(account_list_os)]
        ddr.to_excel(z,sheet_name=f'{program}_DR', index=False)
        dos.to_excel(z,sheet_name=f'{program}_OS', index=False)
        tdr.to_excel(z,sheet_name=f'{program}_Attributes_DR', index=False)
        tos.to_excel(z,sheet_name=f'{program}_Attributes_OS', index=False)
        z.save()
    if program == 'Home Energy Efficiency SOP':
        a = f'''
        SELECT esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}' and UtilityName = 'Oncor' and ReportSectorDescription = 'Residential' and  (measure_description = 'Central Air Conditioner' OR measure_description = 'Central Heat Pump' OR measure_description = 'Ground Source Heat Pump')
        GROUP BY esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        b = pd.read_sql(a, conn)
        b.to_excel(z, sheet_name='Home_Energy_Characterized', index=False)
        ddr = b.sample(n=2)
        dos = b.sample(n=32)
        account_list_dr = ddr['esiid'].tolist()
        account_list_os = dos['esiid'].tolist()
        tdr = y[y['esiid'].isin(account_list_dr)]
        tos = y[y['esiid'].isin(account_list_os)]
        ddr.to_excel(z,sheet_name=f'{program}_DR', index=False)
        dos.to_excel(z,sheet_name=f'{program}_OS', index=False)
        tdr.to_excel(z,sheet_name='Home_Energy_Attributes_DR', index=False)
        tos.to_excel(z,sheet_name='Home_Energy_Attributes_OS', index=False)
        z.save()
    if program == 'Solar PV SOP':
        a = f'''
        SELECT esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}' and UtilityName = 'Oncor' and ReportSectorDescription = 'Residential' and (measure_description = 'Solar PV')
        GROUP BY esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        b = pd.read_sql(a, conn)
        b.to_excel(z, sheet_name=f'{program}_Characterized', index=False)
        ddr = b.sample(n=2)
        dos = b.sample(n=16)
        account_list_dr = ddr['esiid'].tolist()
        account_list_os = dos['esiid'].tolist()
        tdr = y[y['esiid'].isin(account_list_dr)]
        tos = y[y['esiid'].isin(account_list_os)]
        ddr.to_excel(z,sheet_name=f'{program}_DR', index=False)
        dos.to_excel(z,sheet_name=f'{program}_OS', index=False)
        tdr.to_excel(z,sheet_name=f'{program}_Attributes_DR', index=False)
        tos.to_excel(z,sheet_name=f'{program}_Attributes_OS', index=False)
        z.save()
    if program == 'Targeted Weatherization Low-Income SOP':
        a = f'''
        SELECT esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}' and UtilityName = 'Oncor' and (measure_description = 'Air Infiltration' OR measure_description = 'Ceiling Insulation'
        OR measure_description = 'Central Heat Pump' OR measure_description = 'Wall Insulation')
        GROUP BY esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        b = pd.read_sql(a, conn)
        b.to_excel(z, sheet_name='LowIncome_Characterized', index=False)
        ddr = b.sample(n=1)
        dos = b.sample(n=16)
        account_list_dr = ddr['esiid'].tolist()
        account_list_os = dos['esiid'].tolist()
        tdr = y[y['esiid'].isin(account_list_dr)]
        tos = y[y['esiid'].isin(account_list_os)]
        ddr.to_excel(z,sheet_name='LowIncome_DR', index=False)
        dos.to_excel(z,sheet_name='LowIncome_OS', index=False)
        tdr.to_excel(z,sheet_name='LowIncome_Attributes_DR', index=False)
        tos.to_excel(z,sheet_name='LowIncome_Attributes_OS', index=False)
        z.save()
#%% Sampling for SWEPCO

#List of programs to iterate through the sampling
programs = ['Hard-to-Reach SOP','Residential SOP',]

# For loop to iterate through the above programs, creating an initial population df, characterized, choosing samples and sample details. Then writing each df to an excel sheet
for program in programs:
    # query to get inital population 
    
    x = f'''
    SELECT UtilityProgramName, participant_id, participant_code, esiid, customer_name, mail_address1,  program_year_id, UtilityName, ProgramYear, UtilityProgramName
    , ProgramName, ProgramDescription, SectorName, ReportSectorDescription, ProgramTypeName, ProgramGroupName, measure_description, measure_reporting_description
    , source_measure_code, realization_rate_kw, realization_rate_kwh, MeasureCategoryName, project_id, source_data_row_id, participant_impact_date, count, quantity
    , incentive, exante_savings_kw, exante_savings_kwh,trade_ally_zip, UtilityID, ProgramID, SectorID, ProgramTypeID
    , ProgramGroupID, measure_id, quantity_unit, measure_category_id, PrecisionRankName, ReportSectorSort, eul, EvaluationPriority, ReportSectorID
    FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
    where UtilityName = 'SWEPCO' ;
    '''
    
    # Running the above queries in SQL and assiging it to Y and B variable DF
    y = pd.read_sql(x, conn)
    
    
    # Writer variable for writer attribute when using to.excel 
    z =pd.ExcelWriter(output_folder + f"SWEPCO\SWEPCO_{program}.xlsx")
    
    # writing the characterized and initial population df to each files sheet in excel
    y.to_excel(z, sheet_name='Population', index=False)
    
    #iterate through programs to get samples sheet, population to choose samples from and samples attribute sheet. d variable: sample df - t variable: samples with attributes
    if program == 'Hard-to-Reach SOP':
        a = f'''
        SELECT esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}' and UtilityName = 'SWEPCO' and  (measure_description = 'R-AirInf' OR measure_description = 'R-AirPurifier' 
        OR measure_description = 'R-CeilingIns' OR measure_description = 'R-DuctEff' OR measure_description = 'R-DuctEffAlt')
        GROUP BY esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        b = pd.read_sql(a, conn)
        b.to_excel(z, sheet_name=f'{program}_Characterized', index=False)
        ddr = b.sample(n=2)
        dos = b.sample(n=16)
        account_list_dr = ddr['esiid'].tolist()
        account_list_os = dos['esiid'].tolist()
        tdr = y[y['esiid'].isin(account_list_dr)]
        tos = y[y['esiid'].isin(account_list_os)]
        ddr.to_excel(z,sheet_name=f'{program}_DR', index=False)
        dos.to_excel(z,sheet_name=f'{program}_OS', index=False)
        tdr.to_excel(z,sheet_name=f'{program}_Attributes_DR', index=False)
        tos.to_excel(z,sheet_name=f'{program}_Attributes_OS', index=False)
        z.save()
    if program == 'Residential SOP':
        a = f'''
        SELECT esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}' and UtilityName = 'SWEPCO' and  (measure_description = 'ENERGY STAR Pool Pump' OR measure_description = 'R-CentAC' OR measure_description = 'R-CentHP'
        OR measure_description = 'R-DuctEff' OR measure_description = 'R-DuctEffAlt' OR measure_description = 'R-HPWH' OR measure_description = 'R-MSHP')
        GROUP BY esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        b = pd.read_sql(a, conn)
        b.to_excel(z, sheet_name=f'{program}_Characterized', index=False)
        ddr = b.sample(n=2)
        dos = b.sample(n=24)
        account_list_dr = ddr['esiid'].tolist()
        account_list_os = dos['esiid'].tolist()
        tdr = y[y['esiid'].isin(account_list_dr)]
        tos = y[y['esiid'].isin(account_list_os)]
        ddr.to_excel(z,sheet_name=f'{program}_DR', index=False)
        dos.to_excel(z,sheet_name=f'{program}_OS', index=False)
        tdr.to_excel(z,sheet_name=f'{program}_gAttributes_DR', index=False)
        tos.to_excel(z,sheet_name=f'{program}_Attributes_OS', index=False)
        z.save()
#%% Sampling for TNMP

#List of programs to iterate through the sampling
programs = ['Hard-to-Reach SOP','Residential SOP', 'Low Income Weatherization']

# For loop to iterate through the above programs, creating an initial population df, characterized, choosing samples and sample details. Then writing each df to an excel sheet
for program in programs:
    # query to get inital population 
    
    x = f'''
    SELECT UtilityProgramName, participant_id, participant_code, esiid, customer_name, mail_address1,  program_year_id, UtilityName, ProgramYear, UtilityProgramName
    , ProgramName, ProgramDescription, SectorName, ReportSectorDescription, ProgramTypeName, ProgramGroupName, measure_description, measure_reporting_description
    , source_measure_code, realization_rate_kw, realization_rate_kwh, MeasureCategoryName, project_id, source_data_row_id, participant_impact_date, count, quantity
    , incentive, exante_savings_kw, exante_savings_kwh,trade_ally_zip, UtilityID, ProgramID, SectorID, ProgramTypeID
    , ProgramGroupID, measure_id, quantity_unit, measure_category_id, PrecisionRankName, ReportSectorSort, eul, EvaluationPriority, ReportSectorID
    FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
    where UtilityName = 'TNMP' ;
    '''
    
    # Running the above queries in SQL and assiging it to Y and B variable DF
    y = pd.read_sql(x, conn)
    
    
    # Writer variable for writer attribute when using to.excel 
    z =pd.ExcelWriter(output_folder + f"TNMP\TNMP_{program}.xlsx")
    
    # writing the characterized and initial population df to each files sheet in excel
    y.to_excel(z, sheet_name='Population', index=False)
    
    #iterate through programs to get samples sheet, population to choose samples from and samples attribute sheet. d variable: sample df - t variable: samples with attributes
    if program == 'Hard-to-Reach SOP':
        a = f'''
        SELECT esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}' and UtilityName = 'TNMP' and   (measure_description = 'R-CeilingIns' OR measure_description = 'R-DuctEff')
        GROUP BY esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        b = pd.read_sql(a, conn)
        b.to_excel(z, sheet_name=f'{program}_Characterized', index=False)
        ddr = b.sample(n=2)
        dos = b.sample(n=16)
        account_list_dr = ddr['esiid'].tolist()
        account_list_os = dos['esiid'].tolist()
        tdr = y[y['esiid'].isin(account_list_dr)]
        tos = y[y['esiid'].isin(account_list_os)]
        ddr.to_excel(z,sheet_name=f'{program}_DR', index=False)
        dos.to_excel(z,sheet_name=f'{program}_OS', index=False)
        tdr.to_excel(z,sheet_name=f'{program}_Attributes_DR', index=False)
        tos.to_excel(z,sheet_name=f'{program}_Attributes_OS', index=False)
        z.save()
    if program == 'Residential SOP':
        a = f'''
        SELECT esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}' and  UtilityName = 'TNMP' and  (measure_description = 'R-CentAC' OR measure_description = 'R-CentHP')
        GROUP BY esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        a2 = f'''
        SELECT esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}' and  UtilityName = 'TNMP' and  (measure_description = 'R-CeilingIns' OR measure_description = 'R-CentAC' OR measure_description = 'R-CentHP'
        OR measure_description = 'R-DuctEff' OR measure_description = 'Tune Up - Split AC')
        GROUP BY esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        b = pd.read_sql(a, conn)
        b.to_excel(z, sheet_name='RSOP_Characterized_DR', index=False)
        b2 = pd.read_sql(a2, conn)
        b2.to_excel(z, sheet_name='RSOP_Characterized_OS', index=False)
        ddr = b.sample(n=2)
        dos = b2.sample(n=24)
        account_list_dr = ddr['esiid'].tolist()
        account_list_os = dos['esiid'].tolist()
        tdr = y[y['esiid'].isin(account_list_dr)]
        tos = y[y['esiid'].isin(account_list_os)]
        ddr.to_excel(z,sheet_name=f'{program}_DR', index=False)
        dos.to_excel(z,sheet_name=f'{program}_OS', index=False)
        tdr.to_excel(z,sheet_name=f'{program}_Attributes_DR', index=False)
        tos.to_excel(z,sheet_name=f'{program}_Attributes_OS', index=False)
        z.save()
    if program == 'Low Income Weatherization':
        a = f'''
        SELECT esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}' and UtilityName = 'TNMP' and  (measure_description = 'R-CentHP')
        GROUP BY esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        b = pd.read_sql(a, conn)
        b.to_excel(z, sheet_name='LowIncome_Characterized', index=False)
        ddr = b.sample(n=1)
        dos = b.sample(n=16)
        account_list_dr = ddr['esiid'].tolist()
        account_list_os = dos['esiid'].tolist()
        tdr = y[y['esiid'].isin(account_list_dr)]
        tos = y[y['esiid'].isin(account_list_os)]
        ddr.to_excel(z,sheet_name='LowIncome_DR', index=False)
        dos.to_excel(z,sheet_name='LowIncome_OS', index=False)
        tdr.to_excel(z,sheet_name='LowIncome_Attributes_DR', index=False)
        tos.to_excel(z,sheet_name='LowIncome_Attributes_OS', index=False)
        z.save()
#%% Sampling for Xcel

#List of programs to iterate through the sampling
programs = ['Hard-to-Reach SOP','Residential SOP','Low-Income Weatherization' ]

# For loop to iterate through the above programs, creating an initial population df, characterized, choosing samples and sample details. Then writing each df to an excel sheet
for program in programs:
    # query to get inital population 
    
    x = f'''
    SELECT UtilityProgramName, participant_id, participant_code, esiid, customer_name, mail_address1,  program_year_id, UtilityName, ProgramYear, UtilityProgramName
    , ProgramName, ProgramDescription, SectorName, ReportSectorDescription, ProgramTypeName, ProgramGroupName, measure_description, measure_reporting_description
    , source_measure_code, realization_rate_kw, realization_rate_kwh, MeasureCategoryName, project_id, source_data_row_id, participant_impact_date, count, quantity
    , incentive, exante_savings_kw, exante_savings_kwh,trade_ally_zip, UtilityID, ProgramID, SectorID, ProgramTypeID
    , ProgramGroupID, measure_id, quantity_unit, measure_category_id, PrecisionRankName, ReportSectorSort, eul, EvaluationPriority, ReportSectorID
    FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
    where UtilityName = 'Xcel' ;
    '''
    
    # Running the above queries in SQL and assiging it to Y and B variable DF
    y = pd.read_sql(x, conn)
    
    
    # Writer variable for writer attribute when using to.excel 
    z =pd.ExcelWriter(output_folder + f"Xcel\Xcel_{program}.xlsx")
    
    # writing the characterized and initial population df to each files sheet in excel
    y.to_excel(z, sheet_name='Population', index=False)
    
      #iterate through programs to get samples sheet, population to choose samples from and samples attribute sheet. d variable: sample df - t variable: samples with attributes
    if program == 'Hard-to-Reach SOP':
        a = f'''
        SELECT esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}'  and UtilityName = 'Xcel' and (measure_description = 'R-AirInf' OR measure_description = 'R-DuctEff' OR measure_description = 'R-DuctEffAlt')
        GROUP BY esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        b = pd.read_sql(a, conn)
        b.to_excel(z, sheet_name=f'{program}_Characterized', index=False)
        ddr = b.sample(n=1)
        dos = b.sample(n=16)
        account_list_dr = ddr['esiid'].tolist()
        account_list_os = dos['esiid'].tolist()
        tdr = y[y['esiid'].isin(account_list_dr)]
        tos = y[y['esiid'].isin(account_list_os)]
        ddr.to_excel(z,sheet_name=f'{program}_DR', index=False)
        dos.to_excel(z,sheet_name=f'{program}_OS', index=False)
        tdr.to_excel(z,sheet_name=f'{program}_Attributes_DR', index=False)
        tos.to_excel(z,sheet_name=f'{program}_Attributes_OS', index=False)
        z.save()
    if program == 'Residential SOP':
        a = f'''
        SELECT esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}' and  UtilityName = 'Xcel' and (measure_description = 'R-CeilingIns')
        GROUP BY esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        b = pd.read_sql(a, conn)
        b.to_excel(z, sheet_name=f'{program}_Characterized', index=False)
        ddr = b.sample(n=2)
        dos = b.sample(n=16)
        account_list_dr = ddr['esiid'].tolist()
        account_list_os = dos['esiid'].tolist()
        tdr = y[y['esiid'].isin(account_list_dr)]
        tos = y[y['esiid'].isin(account_list_os)]
        ddr.to_excel(z,sheet_name=f'{program}_DR', index=False)
        dos.to_excel(z,sheet_name=f'{program}_OS', index=False)
        tdr.to_excel(z,sheet_name=f'{program}_Attributes_DR', index=False)
        tos.to_excel(z,sheet_name=f'{program}_Attributes_OS', index=False)
        z.save()
    
    if program == 'Low-Income Weatherization':
        a = f'''
        SELECT esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}' and  UtilityName = 'Xcel' and (measure_description = 'HP')
        GROUP BY esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        b = pd.read_sql(a, conn)
        b.to_excel(z, sheet_name='Low_Income_Characterized', index=False)
        ddr = b.sample(n=1)
        dos = b.sample(n=16)
        account_list_dr = ddr['esiid'].tolist()
        account_list_os = dos['esiid'].tolist()
        tdr = y[y['esiid'].isin(account_list_dr)]
        tos = y[y['esiid'].isin(account_list_os)]
        ddr.to_excel(z,sheet_name=f'{program}_DR', index=False)
        dos.to_excel(z,sheet_name=f'{program}_OS', index=False)
        tdr.to_excel(z,sheet_name='Low_Income_Attributes_DR', index=False)
        tos.to_excel(z,sheet_name='Low_Income_Attributes_OS', index=False)
        z.save()
#%% Sampling for El Paso

#List of programs to iterate through the sampling
programs = ['Hard-to-Reach Solutions MTP','Residential Solutions MTP' ]

# For loop to iterate through the above programs, creating an initial population df, characterized, choosing samples and sample details. Then writing each df to an excel sheet
for program in programs:
    # query to get inital population 
    
    x = f'''
    SELECT UtilityProgramName, participant_id, participant_code, esiid, customer_name, mail_address1,  program_year_id, UtilityName, ProgramYear, UtilityProgramName
    , ProgramName, ProgramDescription, SectorName, ReportSectorDescription, ProgramTypeName, ProgramGroupName, measure_description, measure_reporting_description
    , source_measure_code, realization_rate_kw, realization_rate_kwh, MeasureCategoryName, project_id, source_data_row_id, participant_impact_date, count, quantity
    , incentive, exante_savings_kw, exante_savings_kwh,trade_ally_zip, UtilityID, ProgramID, SectorID, ProgramTypeID
    , ProgramGroupID, measure_id, quantity_unit, measure_category_id, PrecisionRankName, ReportSectorSort, eul, EvaluationPriority, ReportSectorID
    FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
    where UtilityName = 'El Paso Electric' ;
    '''
    
    # Running the above queries in SQL and assiging it to Y and B variable DF
    y = pd.read_sql(x, conn)
    
    
    # Writer variable for writer attribute when using to.excel 
    z =pd.ExcelWriter(output_folder + f"El_Paso\El_Paso_{program}.xlsx")
    
    # writing the characterized and initial population df to each files sheet in excel
    y.to_excel(z, sheet_name='Population', index=False)
    
    #iterate through programs to get samples sheet, population to choose samples from and samples attribute sheet. d variable: sample df - t variable: samples with attributes
    if program == 'Hard-to-Reach Solutions MTP':
        a = f'''
        SELECT esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}' and UtilityName = 'El Paso Electric' and (measure_description = 'Ceiling/Roof Insulation: < R5 to R-30' 
        OR measure_description = 'Ceiling/Roof Insulation: R-5-8 to R-30' OR measure_description = 'Ceiling/Roof Insulation: R-9-14 to R-30' OR measure_description = 'Evaporative Cooling')
        GROUP BY esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        b = pd.read_sql(a, conn)
        b.to_excel(z, sheet_name='Hard-to-reach_Characterized', index=False)
        ddr = b.sample(n=0)
        dos = b.sample(n=16)
        account_list_dr = ddr['esiid'].tolist()
        account_list_os = dos['esiid'].tolist()
        tdr = y[y['esiid'].isin(account_list_dr)]
        tos = y[y['esiid'].isin(account_list_os)]
        ddr.to_excel(z,sheet_name='Hard-to-reach_DR', index=False)
        dos.to_excel(z,sheet_name='Hard-to-reach_OS', index=False)
        tdr.to_excel(z,sheet_name='Hard-to-reach_Attributes_DR', index=False)
        tos.to_excel(z,sheet_name='Hard-to-reach_Attributes_OS', index=False)
        z.save()
    if program == 'Residential Solutions MTP':
        a = f'''
        SELECT esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id, sum(exante_savings_kw) as 'Total KW Savings', sum(exante_savings_kwh) as 'Total KWH Savings' 
        FROM TX_EMV_PUCT.TX2022.CustomerImpactExport
        WHERE UtilityProgramName = '{program}' and  UtilityName = 'El Paso Electric' and (measure_description = 'Central Air Conditioner' or measure_description = 'Duct Sealing - Average Leakage'
        or measure_description = 'Evaporative Cooling' or measure_description = 'HVAC-DX/Heat Pump' or measure_description = 'Pool Pump' )
        GROUP BY esiid, UtilityName, UtilityProgramName, ProgramDescription,  program_year_id;
        '''
        b = pd.read_sql(a, conn)
        b.to_excel(z, sheet_name='Res_Characterized', index=False)
        ddr = b.sample(n=0)
        dos = b.sample(n=16)
        account_list_dr = ddr['esiid'].tolist()
        account_list_os = dos['esiid'].tolist()
        tdr = y[y['esiid'].isin(account_list_dr)]
        tos = y[y['esiid'].isin(account_list_os)]
        ddr.to_excel(z,sheet_name=f'{program}_DR', index=False)
        dos.to_excel(z,sheet_name=f'{program}_OS', index=False)
        tdr.to_excel(z,sheet_name='Res__Attributes_DR', index=False)
        tos.to_excel(z,sheet_name='Res__Attributes_OS', index=False)
        z.save()
# %%
